﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Global Common MessageBox Selector for the HiVE Productions [WinForms].")]
[assembly: AssemblyDescription("A WinForms MessageBox selector component which is part of the System.Windows.Forms.MessageBox and Author: Sergey Stoyan, CliverSoft.com Library.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HiVE Productions")]
[assembly: AssemblyProduct("HiVE.MsgBoxSelector [WinForms]")]
[assembly: AssemblyCopyright("Copyright ©  2016-2017 HiVE Productions Inc. All Rights Reserved.")]
[assembly: AssemblyTrademark("HiVE")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("324377e8-70c0-4f4e-a820-ca29bd932a67")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.3.1704.2915")]
[assembly: AssemblyFileVersion("3.3.1704.2915")]
[assembly: NeutralResourcesLanguage("en")]

