﻿using HiVE.MsgBoxSelector.Models;
using System;
using System.Drawing;
using System.Reflection;
using static HiVE.MsgBoxSelector.MsgBoxSpecialMethods;

namespace HiVE.MsgBoxSelector
{
    /// <summary>
    /// Interaction logic for MessageBoxForm.cs
    /// Dynamic dialog box with many answer cases
    /// </summary>
    internal partial class MessageBoxForm : System.Windows.Forms.Form
    {
        public MessageBoxForm()
        {

        }

        #region Fields

        /// <summary>
        /// Vertical span in pixels between controls 
        /// </summary>
        private int SpanY = 12;

        /// <summary>
        /// Horizontal span in pixels between controls 
        /// </summary>
        private int SpanX = 16;

        /// <summary>
        /// Span between button horizontal bound and its text
        /// </summary>
        private int ButtonInternalMarginX = 6;

        /// <summary>
        /// Span between button vertical bound and its text
        /// </summary>
        private int ButtonInternalMarginY = 3;

        /// <summary>
        /// Desired proportion of the message label (not dialog window!) sides
        /// </summary>
        private float MessageBoxWidth2HeightDesiredRegard = 4;

        /// <summary>
        /// Max size of ImageBox.
        /// </summary>
        private int MaxSizeImageBox = 32;

        /// <summary>
        /// Max size of window.
        /// </summary>
        private Size MaxWindowSize = new Size((int)((float)System.Windows.Forms.SystemInformation.PrimaryMonitorMaximizedWindowSize.Width / 1.5), (int)((float)System.Windows.Forms.SystemInformation.PrimaryMonitorMaximizedWindowSize.Height / 1.5));

        /// <summary>
        /// Whether this answer should be repeated automatically
        /// </summary>
        private bool Silent = false;

        /// <summary>
        /// Define whether buttons will be located Vertically or Horisontally
        /// </summary>
        private bool LocateButtons_isVertically = true;

        int bottomEdge = 0;

        /// <summary>
        /// Zero-based index of chosen answer. Cancel = -1
        /// </summary>
        public int Answer = -1;

        #endregion /Fields

        #region Construct Form

        /// <summary>
        /// Get MsgBox Caption Text
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        private string GetMsgBoxCaption(string caption, string culture)
        {
            string msgBoxCaptionText = string.Empty;

            if (caption == null || caption.Trim() == "")
            {
                return string.Empty;
            }
            caption = caption.Trim();

            MsgBoxMethodCaption msgBoxCaption =
                    EnumExtensions.GetEnumValue<MsgBoxMethodCaption>(caption);

            if (msgBoxCaption.ToString().ToUpper() != caption.ToUpper())
            {
                return caption;
            }

            if (msgBoxCaption == MsgBoxMethodCaption.ProductName)
            {
                return (Assembly.GetEntryAssembly().GetName().Name);
            }

            if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
            {
                msgBoxCaptionText = msgBoxCaption.ToDescriptionString();
            }
            /// <summary>
            /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
            /// OR another case
            /// </summary>
            else
            {
                msgBoxCaptionText = msgBoxCaption.ToString();
            }

            return msgBoxCaptionText;
        }

        /// <summary>
        /// Get MsgBox Buttons Text
        /// </summary>
        /// <param name="buttons"></param>
        /// <returns></returns>
        private MsgBoxMethodButtonsText[] GetMsgBoxMethodButtonsText(MsgBoxMethodButtons buttons)
        {
            MsgBoxMethodButtonsText[] msgBoxMethodButtonsText = null;

            switch (buttons)
            {
                #region Common
                //----------

                case MsgBoxMethodButtons.OK:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[1] {
                        MsgBoxMethodButtonsText.OK
                            };
                        break;
                    }
                case MsgBoxMethodButtons.OKCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.OK,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.AbortRetryIgnore:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[3] {
                        MsgBoxMethodButtonsText.Abort,
                        MsgBoxMethodButtonsText.Retry,
                        MsgBoxMethodButtonsText.Ignore
                            };
                        break;
                    }
                case MsgBoxMethodButtons.YesNoCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[3] {
                        MsgBoxMethodButtonsText.Yes,
                        MsgBoxMethodButtonsText.No,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.YesNo:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Yes,
                        MsgBoxMethodButtonsText.No
                            };
                        break;
                    }
                case MsgBoxMethodButtons.RetryCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Retry,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                //----------
                #endregion Common

                #region BackupClient
                //----------

                case MsgBoxMethodButtons.BackupCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Backup,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.BackupDownloadDoNothing:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[3] {
                        MsgBoxMethodButtonsText.Backup,
                        MsgBoxMethodButtonsText.Download,
                        MsgBoxMethodButtonsText.DoNothing
                            };
                        break;
                    }

                //----------
                #endregion BackupClient

                #region ConnectionLost
                //----------

                case MsgBoxMethodButtons.ReconnectExit:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.TryToReconnect,
                        MsgBoxMethodButtonsText.Exit
                            };
                        break;
                    }
                case MsgBoxMethodButtons.TryAgainCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.TryAgain,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.ConnectCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Connect,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                //----------
                #endregion ConnectionLost

                #region FileManager
                //----------

                case MsgBoxMethodButtons.AlreadyExists:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[6] {
                        MsgBoxMethodButtonsText.Overwrite,
                        MsgBoxMethodButtonsText.OverwriteIfOlder,
                        MsgBoxMethodButtonsText.Skip,
                        MsgBoxMethodButtonsText.Rename,
                        MsgBoxMethodButtonsText.Append,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                case MsgBoxMethodButtons.DownloadCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Download,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                case MsgBoxMethodButtons.UploadCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Upload,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                //----------
                #endregion FileManager

                /// if (buttons == MsgBoxMethodButtons.Custom) OR another case
                default:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[1] {
                                MsgBoxMethodButtonsText.Custom
                            };
                        break;
                    }
            }

            return msgBoxMethodButtonsText;
        }

        /// <summary>
        /// Get MsgBox Buttons
        /// </summary>
        /// <param name="msgBoxMethodButtonsText"></param>
        /// <param name="customButtons"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        private string[] GetMsgBoxButtonsText(MsgBoxMethodButtonsText[] msgBoxMethodButtonsText, string[] customButtons, string culture)
        {
            string[] msgBoxMethodButtons = null;

            if (msgBoxMethodButtonsText != null && msgBoxMethodButtonsText.Length > 0)
            {
                msgBoxMethodButtons = new string[msgBoxMethodButtonsText.Length];

                if (msgBoxMethodButtonsText[0] == MsgBoxMethodButtonsText.Custom)
                {
                    if (customButtons != null && customButtons.Length > 0)
                    {
                        string[] msgBoxMethodButtonsTemp = new string[customButtons.Length];
                        int msgBoxMethodButtonsCounter = -1;

                        for (int i = 0; i < customButtons.Length; i++)
                        {
                            if (customButtons[i] != null && customButtons[i].Trim() != "")
                            {
                                msgBoxMethodButtonsTemp[++msgBoxMethodButtonsCounter] = customButtons[i].Trim();
                            }
                        }
                        if (msgBoxMethodButtonsCounter >= 0)
                        {
                            msgBoxMethodButtons = new string[msgBoxMethodButtonsCounter + 1];
                            for (int i = 0; i <= msgBoxMethodButtonsCounter; i++)
                            {
                                msgBoxMethodButtons[i] = msgBoxMethodButtonsTemp[i];
                            }

                            return msgBoxMethodButtons;
                        }
                        else
                        {
                            msgBoxMethodButtons = new string[1];
                            msgBoxMethodButtonsText = new MsgBoxMethodButtonsText[1];
                            msgBoxMethodButtonsText[0] = MsgBoxMethodButtonsText.OK;
                        }
                    }
                    else
                    {
                        msgBoxMethodButtons = new string[1];
                        msgBoxMethodButtonsText = new MsgBoxMethodButtonsText[1];
                        msgBoxMethodButtonsText[0] = MsgBoxMethodButtonsText.OK;
                    }
                }

                if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
                {
                    for (int i = 0; i < msgBoxMethodButtonsText.Length; i++)
                    {
                        msgBoxMethodButtons[i] = msgBoxMethodButtonsText[i].ToDescriptionString();
                    }
                }
                /// <summary>
                /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
                /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
                /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
                /// OR another case
                /// </summary>
                else
                {
                    for (int i = 0; i < msgBoxMethodButtonsText.Length; i++)
                    {
                        msgBoxMethodButtons[i] = msgBoxMethodButtonsText[i].ToSpecialDescriptionString();
                    }
                }
            }

            return msgBoxMethodButtons;
        }

        /// <summary>
        /// Get MsgBox Method United Colors To Colors
        /// </summary>
        /// <param name="msgBoxMethodUnitedColors"></param>
        /// <returns></returns>
        private Color[] GetMsgBoxMethodUnitedColorsToColors(MsgBoxMethodUnitedColors[] msgBoxMethodUnitedColors)
        {
            if (msgBoxMethodUnitedColors == null || msgBoxMethodUnitedColors.Length < 1)
            {
                return null;
            }

            Color[] colors = null;
            colors = new Color[msgBoxMethodUnitedColors.Length];

            for (int i = 0; i < msgBoxMethodUnitedColors.Length; i++)
            {
                try
                {
                    colors[i] = (Color)ColorTranslator.FromHtml(msgBoxMethodUnitedColors[i].ToDescriptionString());
                }
                catch { colors[i] = Color.FromArgb(0, 0, 0, 0); }
            }

            return colors;
        }

        /// <summary>
        /// Get MsgBox United Colors
        /// btnNew.ForeColor = MsgBoxMethodUnitedColors.Default;
        /// btnNew.ForeColor = MsgBoxMethodUnitedColors.Danger;
        /// </summary>
        /// <param name="buttons"></param>
        /// <returns></returns>
        private MsgBoxMethodUnitedColors[] GetMsgBoxMethodUnitedColors(MsgBoxMethodButtonsText[] msgBoxMethodButtonsText)
        {
            MsgBoxMethodUnitedColors[] msgBoxMethodUnitedColors = null;

            if (msgBoxMethodButtonsText != null && msgBoxMethodButtonsText.Length > 0)
            {
                msgBoxMethodUnitedColors = new MsgBoxMethodUnitedColors[msgBoxMethodButtonsText.Length];

                for (int i = 0; i < msgBoxMethodButtonsText.Length; i++)
                {
                    #region MsgBoxMethodButtonsText

                    switch (msgBoxMethodButtonsText[i])
                    {
                        /// Common
                        case MsgBoxMethodButtonsText.OK:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Primary;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Cancel:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Warning;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Abort:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Warning;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Retry:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Link;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Ignore:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Inverse;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Yes:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Primary;
                                break;
                            }
                        case MsgBoxMethodButtonsText.No:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Danger;
                                break;
                            }

                        /// BackupClient
                        case MsgBoxMethodButtonsText.Backup:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Surprise;
                                break;
                            }
                        case MsgBoxMethodButtonsText.DoNothing:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Normal;
                                break;
                            }

                        /// ConnectionLost
                        case MsgBoxMethodButtonsText.TryToReconnect:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Surprise;
                                break;
                            }
                        case MsgBoxMethodButtonsText.TryAgain:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Default;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Connect:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Link;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Exit:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Danger;
                                break;
                            }

                        /// FileManager
                        /// AlreadyExists, Download, Upload
                        case MsgBoxMethodButtonsText.Overwrite:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Danger;
                                break;
                            }
                        case MsgBoxMethodButtonsText.OverwriteIfOlder:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Normal;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Skip:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Inverse;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Rename:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Info;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Append:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Default;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Download:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Success;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Upload:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Primary;
                                break;
                            }

                        /// Custom by user
                        case MsgBoxMethodButtonsText.Custom:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.None;
                                break;
                            }
                    }

                    #endregion MsgBoxMethodButtonsText
                }
            }

            return msgBoxMethodUnitedColors;
        }

        /// <summary>
        /// Get MsgBox Buttons Colors
        /// btnNew.BackColor = msgBoxMethodUnitedColors[buttonsCounter];
        /// btnNew.ForeColor = System.Drawing.Colors;
        /// </summary>
        /// <param name="msgBoxMethodButtonsText"></param>
        /// <param name="buttonsColors"></param>
        /// <param name="customButtonsColors"></param>
        /// <returns></returns>
        private Color[] GetMsgBoxButtonsColors(
            MsgBoxMethodButtonsText[] msgBoxMethodButtonsText,
            MsgBoxMethodButtonsColors buttonsColors,
            Color[] customButtonsColors)
        {
            Color[] msgBoxMethodButtonsColors = null;

            /// Main colors of buttons in application!
            if (buttonsColors == MsgBoxMethodButtonsColors.None) { return null; }

            /// Default colors for buttons in this component!
            else if (buttonsColors == MsgBoxMethodButtonsColors.Default)
            {
                MsgBoxMethodUnitedColors[] msgBoxMethodUnitedColors = GetMsgBoxMethodUnitedColors(msgBoxMethodButtonsText);

                if (msgBoxMethodUnitedColors == null || msgBoxMethodUnitedColors.Length < 1) { return null; }
                else { msgBoxMethodButtonsColors = new Color[msgBoxMethodUnitedColors.Length]; }

                msgBoxMethodButtonsColors = GetMsgBoxMethodUnitedColorsToColors(msgBoxMethodUnitedColors);
            }

            /// Set colors for buttons by user!
            else if (buttonsColors == MsgBoxMethodButtonsColors.Custom)
            {
                msgBoxMethodButtonsColors = customButtonsColors;
            }

            return msgBoxMethodButtonsColors;
        }

        /// <summary>
        /// Get MsgBox Default Button
        /// </summary>
        /// <param name="defaultButton"></param>
        /// <param name="msgBoxMethodButtons_Length"></param>
        /// <returns></returns>
        private int GetMsgBoxDefaultButton(MsgBoxMethodDefaultButton defaultButton, int msgBoxMethodButtons_Length)
        {
            int msgBoxDefaultButton = -1;

            msgBoxDefaultButton = EnumExtensions.GetEnumMemberIndex(defaultButton);

            if (msgBoxDefaultButton < 0) { msgBoxDefaultButton = 0; }
            else if (msgBoxDefaultButton >= msgBoxMethodButtons_Length) { msgBoxDefaultButton = msgBoxMethodButtons_Length - 1; }

            return msgBoxDefaultButton;
        }

        /// <summary>
        /// Set MsgBox Icon_ImageBox
        /// </summary>
        /// <param name="icon"></param>
        /// <param name="customIcon"></param>
        /// <param name="nextPoint"></param>
        /// <returns></returns>
        private Point SetMsgBoxIcon_ImageBox(MsgBoxMethodIcon icon, Image customIcon, Point nextPoint)
        {
            Image image = null;

            try
            {
                switch (icon)
                {
                    case MsgBoxMethodIcon.Asterisk:
                        {
                            image = (Image)SystemIcons.Asterisk.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.Error:
                        {
                            image = (Image)SystemIcons.Error.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.Exclamation:
                        {
                            image = (Image)SystemIcons.Exclamation.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.Hand:
                        {
                            image = (Image)SystemIcons.Hand.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.Information:
                        {
                            image = (Image)SystemIcons.Information.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.None:
                        {
                            image = null;
                            break;
                        }
                    case MsgBoxMethodIcon.Question:
                        {
                            image = (Image)SystemIcons.Question.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.Stop:
                        {
                            image = (Image)SystemIcons.Hand.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.Warning:
                        {
                            image = (Image)SystemIcons.Warning.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.Shield:
                        {
                            image = (Image)SystemIcons.Shield.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.WinLogo:
                        {
                            image = (Image)SystemIcons.WinLogo.ToBitmap();
                            break;
                        }
                    case MsgBoxMethodIcon.ApplicationIcon:
                        {
                            image = (Image)IconRoutines.ExtractIconFromLibrary(Assembly.GetEntryAssembly().Location, false).ToBitmap();
                            break;
                        }

                    case MsgBoxMethodIcon.Custom:
                        {
                            if (customIcon != null)
                            {
                                image = customIcon;
                            }
                            break;
                        }
                    default:
                        {
                            image = null;
                            break;
                        }
                }
            }
            catch { }

            if (image != null)
            {
                this.imageIcon.BackgroundImage = image;

                int maxSize = image.Height;
                if (image.Width > maxSize) { maxSize = image.Width; }

                if (maxSize != MaxSizeImageBox)
                {
                    double height = (image.Height * MaxSizeImageBox / maxSize);
                    double width = (image.Width * MaxSizeImageBox / maxSize);

                    imageIcon.Height = (int)height;
                    imageIcon.Width = (int)width;
                }
                else { this.imageIcon.Size = image.Size; }

                nextPoint.X = this.imageIcon.Right + SpanX;
                nextPoint.Y = this.imageIcon.Bottom + SpanY;
            }
            else
            {
                this.Controls.Remove(imageIcon);
                imageIcon.Dispose();
                imageIcon = null;
                nextPoint.X = this.buttonSample.Left;
                nextPoint.Y = 0;
            }

            return nextPoint;
        }

        /// <summary>
        /// Calculate Buttons Size
        /// United Sise
        /// </summary>
        /// <param name="msgBoxMethodButtons"></param>
        /// <returns></returns>
        private bool CalculateButtonsSize(string[] msgBoxMethodButtons)
        {
            Size max_text_size = new Size(0, 0);

            using (Graphics graphics = buttonSample.CreateGraphics())
            {
                int button_max_width = MaxWindowSize.Width - buttonSample.Left * 2;
                foreach (string btn in msgBoxMethodButtons)
                {
                    SizeF sf = graphics.MeasureString(btn, buttonSample.Font, button_max_width);
                    if (sf.Width > max_text_size.Width)
                        max_text_size.Width = (int)sf.Width;
                    if (sf.Height > max_text_size.Height)
                        max_text_size.Height = (int)sf.Height;
                }
            }

            /// Button size cannot be decreased
            if (buttonSample.Width < max_text_size.Width + 2 * ButtonInternalMarginX)
            {
                buttonSample.Width = max_text_size.Width + 2 * ButtonInternalMarginX;
            }
            if (buttonSample.Height < max_text_size.Height + 2 * ButtonInternalMarginY)
            {
                buttonSample.Height = max_text_size.Height + 2 * ButtonInternalMarginY;
            }

            return true;
        }

        /// <summary>
        /// Define whether buttons will be located Vertically or Horisontally
        /// </summary>
        /// <param name="msgBoxMethodButtons_Length"></param>
        private bool SetLocateButtons(int msgBoxMethodButtons_Length)
        {
            /// Define whether buttons will be located vertically
            int width = buttonSample.Left + buttonSample.Width * msgBoxMethodButtons_Length + SpanX * (msgBoxMethodButtons_Length - 1) + buttonSample.Left;

            if (width > MaxWindowSize.Width)
            {
                /// Locate Buttons Vertically
                LocateButtons_isVertically = true;
                width = buttonSample.Left + buttonSample.Width + buttonSample.Left;
                if (width >= this.ClientSize.Width)
                {
                    //this.Width = width;
                    this.ClientSize = new Size(width, this.ClientSize.Height);
                }
            }
            else
            {
                /// Locate Buttons Horisontally
                LocateButtons_isVertically = false;
                if (width >= this.ClientSize.Width)
                {
                    //this.Width = width;
                    this.ClientSize = new Size(width, this.ClientSize.Height);
                }
            }

            return true;
        }

        /// <summary>
        /// Calculate label size and enhance window width
        /// </summary>
        /// <param name="text"></param>
        /// <param name="msgBoxMethodButtons_Length"></param>
        /// <param name="nextPoint"></param>
        /// <returns></returns>
        private Point CalculateLabelSize_Text(string text, int msgBoxMethodButtons_Length, Point nextPoint)
        {
            /// Calculate label Text size
            int label_right_margin = this.buttonSample.Left; //this.Width - this.label.Right;//this.image.Right + SpanX;//

            if (imageIcon == null)
            {
                label_right_margin = this.buttonSample.Left;
            }

            richTextBoxMessage.Left = nextPoint.X;
            Graphics graphics = richTextBoxMessage.CreateGraphics();

            int label_min_width = this.ClientSize.Width - richTextBoxMessage.Left - label_right_margin;
            SizeF l_size = graphics.MeasureString(text, richTextBoxMessage.Font, label_min_width, StringFormat.GenericDefault);
            int label_desired_width = (int)Math.Sqrt(l_size.Width * l_size.Height * MessageBoxWidth2HeightDesiredRegard);

            if (label_desired_width > label_min_width)
            {
                int label_max_width = MaxWindowSize.Width - richTextBoxMessage.Left - label_right_margin;
                if (label_desired_width > label_max_width)
                {
                    label_desired_width = label_max_width;
                }

                l_size = graphics.MeasureString(text, richTextBoxMessage.Font, label_desired_width, StringFormat.GenericDefault);

                //int label_max_height = (int)(l_size.Width / MessageBoxWidth2HeightDesiredRegard);
                if (l_size.Width > label_min_width)
                {
                    this.richTextBoxMessage.Width = (int)l_size.Width;
                }
                else
                {
                    this.richTextBoxMessage.Width = label_min_width;
                }
            }
            else
            {
                this.richTextBoxMessage.Width = label_min_width;
            }

            this.richTextBoxMessage.Height = (int)l_size.Height;

            /// Enhance window width
            this.ClientSize = new Size(richTextBoxMessage.Right + label_right_margin, this.ClientSize.Height);

            if (LocateButtons_isVertically)
            {
                buttonSample.Width = this.ClientSize.Width - buttonSample.Left - buttonSample.Left;
            }
            else
            {
                buttonSample.Left = (int)(((float)(this.ClientSize.Width - buttonSample.Width * msgBoxMethodButtons_Length - SpanX * (msgBoxMethodButtons_Length - 1))) / 2);
            }

            return nextPoint;
        }

        /// <summary>
        /// Get MsgBox SilentBox Text
        /// </summary>
        /// <param name="silentBox"></param>
        /// <param name="customSilentBoxText"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        private string GetMsgBoxSilentBoxText(MsgBoxMethodSilentBox silentBox, string customSilentBoxText, string culture)
        {
            if (silentBox == MsgBoxMethodSilentBox.Custom)
            {
                if (customSilentBoxText != null || customSilentBoxText.Trim() != "")
                {
                    return customSilentBoxText.Trim();
                }

                return string.Empty;
            }

            if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
            {
                return (silentBox.ToDescriptionString());
            }
            /// <summary>
            /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
            /// OR another case
            /// </summary>
            else
            {
                return (silentBox.ToSpecialDescriptionString());
            }
        }

        /// <summary>
        /// Set Tune Window Height
        /// </summary>
        /// <param name="text"></param>
        /// <param name="msgBoxMethodButtons_Length"></param>
        /// <param name="msgBoxSilentBoxText"></param>
        /// <param name="nextPoint"></param>
        /// <returns></returns>
        private Point SetTuneWindowHeight(string text, int msgBoxMethodButtons_Length, string msgBoxSilentBoxText, Point nextPoint)
        {
            int general_height = imageIcon != null ? this.imageIcon.Bottom : nextPoint.Y;

            if (general_height < richTextBoxMessage.Top + this.richTextBoxMessage.Height)
            {
                general_height += richTextBoxMessage.Top + this.richTextBoxMessage.Height;
            }

            if (LocateButtons_isVertically)
            {
                general_height += msgBoxMethodButtons_Length * (buttonSample.Height + SpanY);
            }
            else
            {
                general_height += buttonSample.Height + SpanY;
            }

            if (msgBoxSilentBoxText != null)
            {
                general_height += checkBoxSilentBox.Height;
            }

            if (general_height < labelCopyright.Top + this.labelCopyright.Height)
            {
                general_height += labelCopyright.Top + this.labelCopyright.Height;
            }

            general_height += bottomEdge;

            if (general_height > MaxWindowSize.Height)
            {
                int diff = general_height - MaxWindowSize.Height;
                if (diff + 200 < this.richTextBoxMessage.Height)
                    this.richTextBoxMessage.Height -= diff;
                else
                    this.richTextBoxMessage.Height = 200;
            }

            richTextBoxMessage.Text = text;

            return nextPoint;
        }

        /// <summary>
        /// Set MsgBox Buttons
        /// </summary>
        /// <param name="msgBoxMethodButtons"></param>
        /// <param name="msgBoxMethodButtonsText"></param>
        /// <param name="msgBoxMethodButtonsColors"></param>
        /// <param name="nextPoint"></param>
        /// <returns></returns>
        private Point SetMsgBoxButtons(
            string[] msgBoxMethodButtons, MsgBoxMethodButtonsText[] msgBoxMethodButtonsText,
            Color[] msgBoxMethodButtonsColors,
            Point nextPoint)
        {
            int buttonsCounter = 0;
            int customButtonsCounter = 1000;
            System.Windows.Forms.Button buttonNew = new System.Windows.Forms.Button();

            foreach (string btn_Text in msgBoxMethodButtons)
            {
                if (btn_Text != null && btn_Text.Trim() != "")
                {
                    buttonNew = new System.Windows.Forms.Button();

                    buttonNew.ForeColor = ColorTranslator.FromHtml(MsgBoxMethodUnitedColors.Danger.ToDescriptionString());

                    if (msgBoxMethodButtonsColors.Length > buttonsCounter)
                    {
                        buttonNew.BackColor = msgBoxMethodButtonsColors[buttonsCounter];

                        if (msgBoxMethodButtonsColors[buttonsCounter] != ColorTranslator.FromHtml(MsgBoxMethodUnitedColors.None.ToDescriptionString()) &&
                            msgBoxMethodButtonsColors[buttonsCounter] != ColorTranslator.FromHtml(MsgBoxMethodUnitedColors.Default.ToDescriptionString()) &&
                            msgBoxMethodButtonsColors[buttonsCounter] != ColorTranslator.FromHtml(MsgBoxMethodUnitedColors.Warning.ToDescriptionString()) &&
                            msgBoxMethodButtonsColors[buttonsCounter] != ColorTranslator.FromHtml(MsgBoxMethodUnitedColors.Normal.ToDescriptionString()))
                        {
                            buttonNew.ForeColor = ColorTranslator.FromHtml(MsgBoxMethodUnitedColors.Default.ToDescriptionString());
                        }
                    }

                    buttonNew.Name = "button" + buttonsCounter.ToString();
                    buttonNew.Size = buttonSample.Size;
                    buttonNew.TabIndex = buttonsCounter + 2;
                    buttonNew.Text = btn_Text.Trim();
                    //btnNew.UseVisualStyleBackColor = false;
                    buttonNew.Click += new System.EventHandler(this.Button_Click);

                    if (msgBoxMethodButtonsText.Length == msgBoxMethodButtons.Length)
                    {
                        buttonNew.Tag = EnumExtensions.GetEnumMemberIndex(msgBoxMethodButtonsText[buttonsCounter]);
                    }
                    else
                    {
                        buttonNew.Tag = ++customButtonsCounter;
                    }

                    buttonNew.Location = nextPoint;
                    if (LocateButtons_isVertically) { nextPoint.Y += buttonNew.Height + SpanY; }
                    else
                    {
                        nextPoint.X += buttonNew.Width + SpanX;
                    }

                    this.Controls.Add(buttonNew);
                    buttonsCounter++;
                }
            }

            if (!LocateButtons_isVertically)
            {
                nextPoint.Y += buttonSample.Height + SpanY;
            }

            this.Controls.Remove(buttonSample);
            buttonSample.Dispose();
            buttonSample = null;

            return nextPoint;
        }

        /// <summary>
        /// Set Silent CheckBox
        /// </summary>
        /// <param name="msgBoxSilentBoxText"></param>
        /// <param name="nextPoint"></param>
        /// <returns></returns>
        private Point SetSilentCheckBox(string msgBoxSilentBoxText, Point nextPoint)
        {
            if (msgBoxSilentBoxText != null && msgBoxSilentBoxText != "")
            {
                Graphics graphics = checkBoxSilentBox.CreateGraphics();
                SizeF sf = graphics.MeasureString(msgBoxSilentBoxText, checkBoxSilentBox.Font, this.ClientRectangle.Width - SpanX - SpanX - checkBoxSilentBox.Width);
                Size s_size = checkBoxSilentBox.Size;
                checkBoxSilentBox.AutoSize = false;
                checkBoxSilentBox.Width = s_size.Width + (int)sf.Width;
                checkBoxSilentBox.Height = (int)sf.Height + 4;
                checkBoxSilentBox.Left = (int)((float)(this.ClientRectangle.Width - checkBoxSilentBox.Width) / 2);
                checkBoxSilentBox.Top = nextPoint.Y;
                nextPoint.Y = checkBoxSilentBox.Bottom + bottomEdge;
                checkBoxSilentBox.Text = msgBoxSilentBoxText;
            }
            else
            {
                this.Controls.Remove(checkBoxSilentBox);
                checkBoxSilentBox.Dispose();
                checkBoxSilentBox = null;
                nextPoint.Y += bottomEdge - SpanY;
            }

            return nextPoint;
        }

        /// <summary>
        /// ChangeCulture
        /// Design-FlowDirection RTL or LTR
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        private bool ChangeCulture_FlowDirection(string culture)
        {
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;

            /// <summary>
            /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
            /// OR another case
            /// </summary>
            System.Windows.Forms.RightToLeft _flowDirection = System.Windows.Forms.RightToLeft.No;

            if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
            {
                _flowDirection = System.Windows.Forms.RightToLeft.Yes;
            }

            try
            {
                if (_flowDirection == System.Windows.Forms.RightToLeft.Yes) { this.RightToLeftLayout = true; }
                else { this.RightToLeftLayout = false; }

                richTextBoxMessage.RightToLeft = _flowDirection;

                labelCopyright.RightToLeft = System.Windows.Forms.RightToLeft.No;

                richTextBoxMessage.Focus();
            }
            catch { return false; }

            return true;
        }

        //----------
        #endregion /Construct Form

        #region Base Methods

        /// <summary>
        /// Button_Click event for sample button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (System.Windows.Forms.Button)sender;
            Answer = (int)btn.Tag;
            if (checkBoxSilentBox != null)
            {
                Silent = checkBoxSilentBox.Checked;
            }
            this.Close();
        }

        /// <summary>
        /// Construct dynamic form with many cases
        /// </summary>
        /// <param name="text">message information</param>
        /// <param name="caption">window caption</param>
        /// <param name="buttons">array of possible answers</param>
        /// <param name="customButtons">custom array of possible answers</param>
        /// <param name="icon">image to be displayed as the message icon</param>
        /// <param name="customIcon">custom image to be displayed as the message icon</param>
        /// <param name="defaultButton">default answer zero-based index</param>
        /// <param name="options">options</param>
        /// <param name="buttonsColors">button colors</param>
        /// <param name="customButtonsColors">custom buttons colors</param>
        /// <param name="silentBox">silent check box; if None then check box will not be displayed</param>
        /// <param name="customSilentBoxText">custom silent check box text; if null then check box will not be displayed</param>
        /// <param name="culture">change culture and change design RTL or LTR</param>
        private void construct_Form(
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, Image customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture)
        {
            /// <summary>
            /// Define Product Copyright for this component
            /// </summary>
            labelCopyright.Text = AssemblyAttributeAccessors.ExecutingAssemblyCopyright;

            if (text == null)
            {
                text = string.Empty;
            }
            text = text.Trim();

            if (caption == null)
            {
                caption = string.Empty;
            }
            caption = caption.Trim();

            if (customSilentBoxText == null)
            {
                customSilentBoxText = string.Empty;
            }
            customSilentBoxText = customSilentBoxText.Trim();

            if (culture == null)
            {
                culture = string.Empty;
            }
            culture = culture.Trim();

            this.Text = GetMsgBoxCaption(caption, culture);

            MsgBoxMethodButtonsText[] msgBoxMethodButtonsText = GetMsgBoxMethodButtonsText(buttons);
            string[] msgBoxButtons = GetMsgBoxButtonsText(msgBoxMethodButtonsText, customButtons, culture);

            Color[] msgBoxButtonsColors = GetMsgBoxButtonsColors(msgBoxMethodButtonsText, buttonsColors, customButtonsColors);
            if (msgBoxButtonsColors == null) { msgBoxButtonsColors = new Color[0]; }

            int messageBoxDefaultButton = GetMsgBoxDefaultButton(defaultButton, msgBoxButtons.Length);

            Point nextPoint = new Point(0, 0);

            nextPoint = SetMsgBoxIcon_ImageBox(icon, customIcon, nextPoint);

            CalculateButtonsSize(msgBoxButtons);

            SetLocateButtons(msgBoxButtons.Length);

            nextPoint = CalculateLabelSize_Text(text, msgBoxButtons.Length, nextPoint);

            bottomEdge = this.ClientSize.Height - checkBoxSilentBox.Bottom;

            string messageBoxSilentBoxText = GetMsgBoxSilentBoxText(silentBox, customSilentBoxText, culture);

            nextPoint = SetTuneWindowHeight(text, msgBoxButtons.Length, messageBoxSilentBoxText, nextPoint);

            if (nextPoint.Y < richTextBoxMessage.Bottom + SpanY)
                nextPoint.Y = richTextBoxMessage.Bottom + SpanY;

            nextPoint.X = buttonSample.Left;

            nextPoint = SetMsgBoxButtons(msgBoxButtons, msgBoxMethodButtonsText, msgBoxButtonsColors, nextPoint);

            nextPoint = SetSilentCheckBox(messageBoxSilentBoxText, nextPoint);

            this.ClientSize = new Size(this.ClientSize.Width, nextPoint.Y);
            if (this.Height > MaxWindowSize.Height)
            {
                this.Size = new Size(this.Size.Width, MaxWindowSize.Height);
                this.AutoScroll = true;
            }

            ChangeCulture_FlowDirection(culture);

            this.Controls["button" + messageBoxDefaultButton.ToString()].Select();
        }

        /// <summary>
        /// Construct dynamic dialog box with many answer cases
        /// </summary>
        /// <param name="text">message information</param>
        /// <param name="caption">window caption</param>
        /// <param name="buttons">array of possible answers</param>
        /// <param name="customButtons">custom array of possible answers</param>
        /// <param name="icon">image to be displayed as the message icon</param>
        /// <param name="customIcon">custom image to be displayed as the message icon</param>
        /// <param name="defaultButton">default answer zero-based index</param>
        /// <param name="options">options</param>
        /// <param name="buttonsColors">button colors</param>
        /// <param name="customButtonsColors">custom buttons colors</param>
        /// <param name="silentBox">silent check box; if None then check box will not be displayed</param>
        /// <param name="customSilentBoxText">custom silent check box text; if null then check box will not be displayed</param>
        /// <param name="culture">change culture and change design RTL or LTR</param>
        public MessageBoxForm(
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, Image customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture)
        {
            InitializeComponent();

            try
            {
                construct_Form(
                    text,
                    caption,
                    buttons, customButtons,
                    icon, customIcon,
                    defaultButton,
                    options,
                    buttonsColors, customButtonsColors,
                    silentBox, customSilentBoxText,
                    culture);
            }
            catch { }
        }

        /// <summary>
        /// Show MessageForm as a dialog box and get user's answer
        /// </summary>
        /// <returns>zero-based index of chosen answer. Cancel = -1</returns>
        private int GetResult()
        {
            if (this.Owner == null)
            {
                //this.ShowInTaskbar = true;
                System.Windows.Forms.Form messageForm = new System.Windows.Forms.Form(); /// needed to bring message box to front
                messageForm.Icon = this.Icon;
                messageForm.BringToFront();
                this.ShowDialog(messageForm);
            }
            else
            {
                this.ShowDialog();
            }

            return Answer;
        }

        /// <summary>
        /// Show MessageForm as a dialog box and return user's answer
        /// </summary>
        /// <returns>zero-based index of chosen answer. Cancel = -1</returns>
        public int GetDialogResult()
        {
            return GetResult();
        }

        #endregion /Base Methods
    }
}
