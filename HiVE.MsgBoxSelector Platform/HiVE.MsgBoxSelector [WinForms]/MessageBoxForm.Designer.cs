﻿namespace HiVE.MsgBoxSelector
{
    partial class MessageBoxForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxMessage = new System.Windows.Forms.RichTextBox();
            this.checkBoxSilentBox = new System.Windows.Forms.CheckBox();
            this.buttonSample = new System.Windows.Forms.Button();
            this.imageIcon = new System.Windows.Forms.PictureBox();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.splitterStripStatusBottom = new System.Windows.Forms.Splitter();
            this.panelStripStatusBottom = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.imageIcon)).BeginInit();
            this.panelStripStatusBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBoxMessage
            // 
            this.richTextBoxMessage.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBoxMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxMessage.Location = new System.Drawing.Point(50, 6);
            this.richTextBoxMessage.Name = "richTextBoxMessage";
            this.richTextBoxMessage.ReadOnly = true;
            this.richTextBoxMessage.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBoxMessage.Size = new System.Drawing.Size(300, 32);
            this.richTextBoxMessage.TabIndex = 15;
            this.richTextBoxMessage.Text = "Message";
            // 
            // checkBoxSilentBox
            // 
            this.checkBoxSilentBox.AutoSize = true;
            this.checkBoxSilentBox.Location = new System.Drawing.Point(17, 74);
            this.checkBoxSilentBox.Name = "checkBoxSilentBox";
            this.checkBoxSilentBox.Size = new System.Drawing.Size(73, 17);
            this.checkBoxSilentBox.TabIndex = 14;
            this.checkBoxSilentBox.Text = "Silent Box";
            this.checkBoxSilentBox.UseVisualStyleBackColor = false;
            // 
            // buttonSample
            // 
            this.buttonSample.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonSample.Location = new System.Drawing.Point(50, 44);
            this.buttonSample.Name = "buttonSample";
            this.buttonSample.Size = new System.Drawing.Size(75, 23);
            this.buttonSample.TabIndex = 13;
            this.buttonSample.Text = "Button";
            this.buttonSample.UseVisualStyleBackColor = false;
            // 
            // imageIcon
            // 
            this.imageIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.imageIcon.Location = new System.Drawing.Point(12, 6);
            this.imageIcon.Name = "imageIcon";
            this.imageIcon.Size = new System.Drawing.Size(32, 32);
            this.imageIcon.TabIndex = 12;
            this.imageIcon.TabStop = false;
            // 
            // labelCopyright
            // 
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelCopyright.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.labelCopyright.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelCopyright.Location = new System.Drawing.Point(0, 5);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Padding = new System.Windows.Forms.Padding(2);
            this.labelCopyright.Size = new System.Drawing.Size(55, 17);
            this.labelCopyright.TabIndex = 9;
            this.labelCopyright.Text = "Copyright";
            this.labelCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splitterStripStatusBottom
            // 
            this.splitterStripStatusBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitterStripStatusBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterStripStatusBottom.Enabled = false;
            this.splitterStripStatusBottom.Location = new System.Drawing.Point(0, 110);
            this.splitterStripStatusBottom.Name = "splitterStripStatusBottom";
            this.splitterStripStatusBottom.Size = new System.Drawing.Size(384, 1);
            this.splitterStripStatusBottom.TabIndex = 17;
            this.splitterStripStatusBottom.TabStop = false;
            // 
            // panelStripStatusBottom
            // 
            this.panelStripStatusBottom.Controls.Add(this.labelCopyright);
            this.panelStripStatusBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStripStatusBottom.Location = new System.Drawing.Point(0, 111);
            this.panelStripStatusBottom.Name = "panelStripStatusBottom";
            this.panelStripStatusBottom.Size = new System.Drawing.Size(384, 22);
            this.panelStripStatusBottom.TabIndex = 16;
            // 
            // messageBoxForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 133);
            this.Controls.Add(this.richTextBoxMessage);
            this.Controls.Add(this.checkBoxSilentBox);
            this.Controls.Add(this.buttonSample);
            this.Controls.Add(this.imageIcon);
            this.Controls.Add(this.splitterStripStatusBottom);
            this.Controls.Add(this.panelStripStatusBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "messageBoxForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MessageBox Form";
            ((System.ComponentModel.ISupportInitialize)(this.imageIcon)).EndInit();
            this.panelStripStatusBottom.ResumeLayout(false);
            this.panelStripStatusBottom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxMessage;
        private System.Windows.Forms.CheckBox checkBoxSilentBox;
        private System.Windows.Forms.Button buttonSample;
        private System.Windows.Forms.PictureBox imageIcon;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Splitter splitterStripStatusBottom;
        private System.Windows.Forms.Panel panelStripStatusBottom;
    }
}