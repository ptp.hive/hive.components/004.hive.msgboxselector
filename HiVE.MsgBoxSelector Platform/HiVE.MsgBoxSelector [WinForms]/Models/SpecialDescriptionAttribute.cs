﻿namespace System.ComponentModel
{
    using System;

    //[AttributeUsage(AttributeTargets.All)]
    //[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    internal class SpecialDescriptionAttribute : Attribute
    {
        public static readonly SpecialDescriptionAttribute Default = new SpecialDescriptionAttribute();
        private string specialDescription;

        public SpecialDescriptionAttribute() : this(string.Empty)
        {
        }

        public SpecialDescriptionAttribute(string especialDescription)
        {
            this.specialDescription = especialDescription;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }
            SpecialDescriptionAttribute attribute = obj as SpecialDescriptionAttribute;
            return ((attribute != null) && (attribute.SpecialDescription == this.SpecialDescription));
        }

        public override int GetHashCode()
        {
            return this.SpecialDescription.GetHashCode();
        }

        public override bool IsDefaultAttribute()
        {
            return this.Equals(Default);
        }

        public virtual string SpecialDescription
        {
            get
            {
                return this.SpecialDescriptionValue;
            }
        }

        protected string SpecialDescriptionValue
        {
            get
            {
                return this.specialDescription;
            }
            set
            {
                this.specialDescription = value;
            }
        }
    }
}