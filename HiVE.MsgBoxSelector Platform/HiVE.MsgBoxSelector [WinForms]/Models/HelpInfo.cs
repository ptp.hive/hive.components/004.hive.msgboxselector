﻿namespace HiVE.MsgBoxSelector.Models
{
    internal class HelpInfo
    {
        // Fields
        private string helpFilePath;
        private string keyword;
        private MsgBoxSpecialMethods.MsgBoxMethodHelpNavigator navigator;
        private int option;
        private object param;

        // Methods
        public HelpInfo(string helpfilepath)
        {
            this.helpFilePath = helpfilepath;
            this.keyword = "";
            this.navigator = MsgBoxSpecialMethods.MsgBoxMethodHelpNavigator.TableOfContents;
            this.param = null;
            this.option = 1;
        }

        public HelpInfo(string helpfilepath, string keyword)
        {
            this.helpFilePath = helpfilepath;
            this.keyword = keyword;
            this.navigator = MsgBoxSpecialMethods.MsgBoxMethodHelpNavigator.TableOfContents;
            this.param = null;
            this.option = 2;
        }

        public HelpInfo(string helpfilepath, MsgBoxSpecialMethods.MsgBoxMethodHelpNavigator navigator)
        {
            this.helpFilePath = helpfilepath;
            this.keyword = "";
            this.navigator = navigator;
            this.param = null;
            this.option = 3;
        }

        public HelpInfo(string helpfilepath, MsgBoxSpecialMethods.MsgBoxMethodHelpNavigator navigator, object param)
        {
            this.helpFilePath = helpfilepath;
            this.keyword = "";
            this.navigator = navigator;
            this.param = param;
            this.option = 4;
        }

        public override string ToString()
        {
            string[] textArray1 = new string[] { "{HelpFilePath=", this.helpFilePath, ", keyword =", this.keyword, ", navigator=", this.navigator.ToString(), "}" };
            return string.Concat(textArray1);
        }

        // Properties
        public string HelpFilePath =>
            this.helpFilePath;

        public string Keyword =>
            this.keyword;

        public MsgBoxSpecialMethods.MsgBoxMethodHelpNavigator Navigator =>
            this.navigator;

        public int Option =>
            this.option;

        public object Param =>
            this.param;
    }

}
