﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace HiVE.MsgBoxSelector.Models
{

    [Serializable, HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    internal class InvalidEnumArgumentException : ArgumentException
    {
        // Fields
        internal const string objArray1 = "InvalidEnumArgument";

        // Methods
        public InvalidEnumArgumentException() : this(null)
        {
        }

        public InvalidEnumArgumentException(string message) : base(message)
        {
        }

        protected InvalidEnumArgumentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public InvalidEnumArgumentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidEnumArgumentException(string argumentName, int invalidValue, Type enumClass) : base(SR.GetString("InvalidEnumArgument", objArray1), argumentName)
        {
            object[] objArray1 = new object[] { argumentName, invalidValue.ToString(CultureInfo.CurrentCulture), enumClass.Name };
        }
    }

}
