﻿using HiVE.MsgBoxSelector.Models;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using static HiVE.MsgBoxSelector.MsgBoxSpecialMethods;

namespace HiVE.MsgBoxSelector
{
    /// <summary>
    /// Interaction logic for MessageBoxWindow.xaml
    /// Dynamic dialog box with many answer cases
    /// </summary>
    internal partial class MessageBoxWindow : Window
    {
        public MessageBoxWindow()
        {
            InitializeComponent();
        }

        #region Fields

        /// <summary>
        /// Whether this answer should be repeated automatically
        /// </summary>
        private bool _SilentBoxIsChecked = false;

        /// <summary>
        /// Define whether buttons will be located Vertically or Horisontally
        /// Default is Orientation.Horizontal;
        /// </summary>
        private Orientation _locateButtonsOrientation = Orientation.Horizontal;

        /// <summary>
        /// Zero-based index of chosen answer. Cancel = -1
        /// </summary>
        public int Answer = -1;

        #endregion Fields

        #region Construct Window

        /// <summary>
        /// Get MsgBox Caption Text
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        private string GetMsgBoxCaption(string caption, string culture)
        {
            string msgBoxCaptionText = string.Empty;

            if (caption == null || caption.Trim() == "")
            {
                return string.Empty;
            }
            caption = caption.Trim();

            MsgBoxMethodCaption msgBoxCaption =
                    EnumExtensions.GetEnumValue<MsgBoxMethodCaption>(caption);

            if (msgBoxCaption.ToString().ToUpper() != caption.ToUpper())
            {
                return caption;
            }

            if (msgBoxCaption == MsgBoxMethodCaption.ProductName)
            {
                return (Assembly.GetEntryAssembly().GetName().Name);
            }

            if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
            {
                msgBoxCaptionText = msgBoxCaption.ToDescriptionString();
            }
            /// <summary>
            /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
            /// OR another case
            /// </summary>
            else
            {
                msgBoxCaptionText = msgBoxCaption.ToString();
            }

            return msgBoxCaptionText;
        }

        /// <summary>
        /// Get MsgBox Buttons Text
        /// </summary>
        /// <param name="buttons"></param>
        /// <returns></returns>
        private MsgBoxMethodButtonsText[] GetMsgBoxMethodButtonsText(MsgBoxMethodButtons buttons)
        {
            MsgBoxMethodButtonsText[] msgBoxMethodButtonsText = null;

            switch (buttons)
            {
                #region Common
                //----------

                case MsgBoxMethodButtons.OK:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[1] {
                        MsgBoxMethodButtonsText.OK
                            };
                        break;
                    }
                case MsgBoxMethodButtons.OKCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.OK,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.AbortRetryIgnore:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[3] {
                        MsgBoxMethodButtonsText.Abort,
                        MsgBoxMethodButtonsText.Retry,
                        MsgBoxMethodButtonsText.Ignore
                            };
                        break;
                    }
                case MsgBoxMethodButtons.YesNoCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[3] {
                        MsgBoxMethodButtonsText.Yes,
                        MsgBoxMethodButtonsText.No,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.YesNo:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Yes,
                        MsgBoxMethodButtonsText.No
                            };
                        break;
                    }
                case MsgBoxMethodButtons.RetryCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Retry,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                //----------
                #endregion Common

                #region BackupClient
                //----------

                case MsgBoxMethodButtons.BackupCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Backup,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.BackupDownloadDoNothing:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[3] {
                        MsgBoxMethodButtonsText.Backup,
                        MsgBoxMethodButtonsText.Download,
                        MsgBoxMethodButtonsText.DoNothing
                            };
                        break;
                    }

                //----------
                #endregion BackupClient

                #region ConnectionLost
                //----------

                case MsgBoxMethodButtons.ReconnectExit:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.TryToReconnect,
                        MsgBoxMethodButtonsText.Exit
                            };
                        break;
                    }
                case MsgBoxMethodButtons.TryAgainCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.TryAgain,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }
                case MsgBoxMethodButtons.ConnectCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Connect,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                //----------
                #endregion ConnectionLost

                #region FileManager
                //----------

                case MsgBoxMethodButtons.AlreadyExists:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[6] {
                        MsgBoxMethodButtonsText.Overwrite,
                        MsgBoxMethodButtonsText.OverwriteIfOlder,
                        MsgBoxMethodButtonsText.Skip,
                        MsgBoxMethodButtonsText.Rename,
                        MsgBoxMethodButtonsText.Append,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                case MsgBoxMethodButtons.DownloadCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Download,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                case MsgBoxMethodButtons.UploadCancel:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[2] {
                        MsgBoxMethodButtonsText.Upload,
                        MsgBoxMethodButtonsText.Cancel
                            };
                        break;
                    }

                //----------
                #endregion FileManager

                /// if (buttons == MsgBoxMethodButtons.Custom) OR another case
                default:
                    {
                        msgBoxMethodButtonsText =
                            new MsgBoxMethodButtonsText[1] {
                                MsgBoxMethodButtonsText.Custom
                            };
                        break;
                    }
            }

            return msgBoxMethodButtonsText;
        }

        /// <summary>
        /// Get MsgBox Buttons
        /// </summary>
        /// <param name="msgBoxMethodButtonsText"></param>
        /// <param name="customButtons"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        private string[] GetMsgBoxButtonsText(MsgBoxMethodButtonsText[] msgBoxMethodButtonsText, string[] customButtons, string culture)
        {
            string[] msgBoxMethodButtons = null;

            if (msgBoxMethodButtonsText != null && msgBoxMethodButtonsText.Length > 0)
            {
                msgBoxMethodButtons = new string[msgBoxMethodButtonsText.Length];

                if (msgBoxMethodButtonsText[0] == MsgBoxMethodButtonsText.Custom)
                {
                    if (customButtons != null && customButtons.Length > 0)
                    {
                        string[] msgBoxMethodButtonsTemp = new string[customButtons.Length];
                        int msgBoxMethodButtonsCounter = -1;

                        for (int i = 0; i < customButtons.Length; i++)
                        {
                            if (customButtons[i] != null && customButtons[i].Trim() != "")
                            {
                                msgBoxMethodButtonsTemp[++msgBoxMethodButtonsCounter] = customButtons[i].Trim();
                            }
                        }
                        if (msgBoxMethodButtonsCounter >= 0)
                        {
                            msgBoxMethodButtons = new string[msgBoxMethodButtonsCounter + 1];
                            for (int i = 0; i <= msgBoxMethodButtonsCounter; i++)
                            {
                                msgBoxMethodButtons[i] = msgBoxMethodButtonsTemp[i];
                            }

                            return msgBoxMethodButtons;
                        }
                        else
                        {
                            msgBoxMethodButtons = new string[1];
                            msgBoxMethodButtonsText = new MsgBoxMethodButtonsText[1];
                            msgBoxMethodButtonsText[0] = MsgBoxMethodButtonsText.OK;
                        }
                    }
                    else
                    {
                        msgBoxMethodButtons = new string[1];
                        msgBoxMethodButtonsText = new MsgBoxMethodButtonsText[1];
                        msgBoxMethodButtonsText[0] = MsgBoxMethodButtonsText.OK;
                    }
                }

                if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
                {
                    for (int i = 0; i < msgBoxMethodButtonsText.Length; i++)
                    {
                        msgBoxMethodButtons[i] = msgBoxMethodButtonsText[i].ToDescriptionString();
                    }
                }
                /// <summary>
                /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
                /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
                /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
                /// OR another case
                /// </summary>
                else
                {
                    for (int i = 0; i < msgBoxMethodButtonsText.Length; i++)
                    {
                        msgBoxMethodButtons[i] = msgBoxMethodButtonsText[i].ToSpecialDescriptionString();
                    }
                }
            }

            return msgBoxMethodButtons;
        }

        /// <summary>
        /// Get MsgBox Method United Colors To Colors
        /// </summary>
        /// <param name="msgBoxMethodUnitedColors"></param>
        /// <returns></returns>
        private Color[] GetMsgBoxMethodUnitedColorsToColors(MsgBoxMethodUnitedColors[] msgBoxMethodUnitedColors)
        {
            if (msgBoxMethodUnitedColors == null || msgBoxMethodUnitedColors.Length < 1)
            {
                return null;
            }

            Color[] colors = null;
            colors = new Color[msgBoxMethodUnitedColors.Length];

            for (int i = 0; i < msgBoxMethodUnitedColors.Length; i++)
            {
                try
                {
                    colors[i] = (Color)ColorConverter.ConvertFromString(msgBoxMethodUnitedColors[i].ToDescriptionString());
                }
                catch { colors[i] = Color.FromArgb(0, 0, 0, 0); }
            }

            return colors;
        }

        /// <summary>
        /// Get MsgBox United Colors
        /// btnNew.Foreground = MsgBoxMethodUnitedColors.Default;
        /// btnNew.Foreground = MsgBoxMethodUnitedColors.Danger;
        /// </summary>
        /// <param name="buttons"></param>
        /// <returns></returns>
        private MsgBoxMethodUnitedColors[] GetMsgBoxMethodUnitedColors(MsgBoxMethodButtonsText[] msgBoxMethodButtonsText)
        {
            MsgBoxMethodUnitedColors[] msgBoxMethodUnitedColors = null;

            if (msgBoxMethodButtonsText != null && msgBoxMethodButtonsText.Length > 0)
            {
                msgBoxMethodUnitedColors = new MsgBoxMethodUnitedColors[msgBoxMethodButtonsText.Length];

                for (int i = 0; i < msgBoxMethodButtonsText.Length; i++)
                {
                    #region MsgBoxMethodButtonsText

                    switch (msgBoxMethodButtonsText[i])
                    {
                        /// Common
                        case MsgBoxMethodButtonsText.OK:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Primary;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Cancel:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Warning;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Abort:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Warning;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Retry:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Link;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Ignore:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Inverse;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Yes:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Primary;
                                break;
                            }
                        case MsgBoxMethodButtonsText.No:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Danger;
                                break;
                            }

                        /// BackupClient
                        case MsgBoxMethodButtonsText.Backup:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Surprise;
                                break;
                            }
                        case MsgBoxMethodButtonsText.DoNothing:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Normal;
                                break;
                            }

                        /// ConnectionLost
                        case MsgBoxMethodButtonsText.TryToReconnect:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Surprise;
                                break;
                            }
                        case MsgBoxMethodButtonsText.TryAgain:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Default;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Connect:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Link;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Exit:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Danger;
                                break;
                            }

                        /// FileManager
                        /// AlreadyExists, Download, Upload
                        case MsgBoxMethodButtonsText.Overwrite:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Danger;
                                break;
                            }
                        case MsgBoxMethodButtonsText.OverwriteIfOlder:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Normal;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Skip:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Inverse;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Rename:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Info;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Append:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Default;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Download:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Success;
                                break;
                            }
                        case MsgBoxMethodButtonsText.Upload:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.Primary;
                                break;
                            }

                        /// Custom by user
                        case MsgBoxMethodButtonsText.Custom:
                            {
                                msgBoxMethodUnitedColors[i] = MsgBoxMethodUnitedColors.None;
                                break;
                            }
                    }

                    #endregion MsgBoxMethodButtonsText
                }
            }

            return msgBoxMethodUnitedColors;
        }

        /// <summary>
        /// Get MsgBox Buttons Colors
        /// btnNew.Background = msgBoxMethodUnitedColors[buttonsCounter];
        /// btnNew.Foreground = System.Windows.Media.Brush;
        /// </summary>
        /// <param name="msgBoxMethodButtonsText"></param>
        /// <param name="buttonsColors"></param>
        /// <param name="customButtonsColors"></param>
        /// <returns></returns>
        private Color[] GetMsgBoxButtonsColors(
            MsgBoxMethodButtonsText[] msgBoxMethodButtonsText,
            MsgBoxMethodButtonsColors buttonsColors,
            Color[] customButtonsColors)
        {
            Color[] msgBoxButtonsColors = null;

            /// Main colors of buttons in application!
            if (buttonsColors == MsgBoxMethodButtonsColors.None) { return null; }

            /// Default colors for buttons in this component!
            else if (buttonsColors == MsgBoxMethodButtonsColors.Default)
            {
                MsgBoxMethodUnitedColors[] msgBoxMethodUnitedColors = GetMsgBoxMethodUnitedColors(msgBoxMethodButtonsText);

                if (msgBoxMethodUnitedColors == null || msgBoxMethodUnitedColors.Length < 1) { return null; }
                else { msgBoxButtonsColors = new Color[msgBoxMethodUnitedColors.Length]; }

                msgBoxButtonsColors = GetMsgBoxMethodUnitedColorsToColors(msgBoxMethodUnitedColors);
            }

            /// Set colors for buttons by user!
            else if (buttonsColors == MsgBoxMethodButtonsColors.Custom)
            {
                msgBoxButtonsColors = customButtonsColors;
            }

            return msgBoxButtonsColors;
        }

        /// <summary>
        /// Get MsgBox Default Button
        /// </summary>
        /// <param name="defaultButton"></param>
        /// <param name="msgBoxMethodButtons_Length"></param>
        /// <returns></returns>
        private int GetMsgBoxDefaultButton(MsgBoxMethodDefaultButton defaultButton, int msgBoxMethodButtons_Length)
        {
            int msgBoxDefaultButton = -1;

            msgBoxDefaultButton = EnumExtensions.GetEnumMemberIndex(defaultButton);

            if (msgBoxDefaultButton < 0) { msgBoxDefaultButton = 0; }
            else if (msgBoxDefaultButton >= msgBoxMethodButtons_Length) { msgBoxDefaultButton = msgBoxMethodButtons_Length - 1; }

            return msgBoxDefaultButton;
        }

        /// <summary>
        /// Set MsgBox Icon_ImageBox
        /// </summary>
        /// <param name="icon"></param>
        /// <param name="customIcon"></param>
        /// <returns></returns>
        private bool SetMsgBoxIcon_ImageBox(MsgBoxMethodIcon icon, System.Windows.Media.Imaging.BitmapImage customIcon)
        {
            ImageSource imageSource = null;

            try
            {
                switch (icon)
                {
                    case MsgBoxMethodIcon.Asterisk:
                        {
                            imageSource = System.Drawing.SystemIcons.Asterisk.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.Error:
                        {
                            imageSource = System.Drawing.SystemIcons.Error.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.Exclamation:
                        {
                            imageSource = System.Drawing.SystemIcons.Exclamation.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.Hand:
                        {
                            imageSource = System.Drawing.SystemIcons.Hand.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.Information:
                        {
                            imageSource = System.Drawing.SystemIcons.Information.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.None:
                        {
                            imageSource = null;
                            break;
                        }
                    case MsgBoxMethodIcon.Question:
                        {
                            imageSource = System.Drawing.SystemIcons.Question.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.Stop:
                        {
                            imageSource = System.Drawing.SystemIcons.Hand.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.Warning:
                        {
                            imageSource = System.Drawing.SystemIcons.Warning.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.Shield:
                        {
                            imageSource = System.Drawing.SystemIcons.Shield.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.WinLogo:
                        {
                            imageSource = System.Drawing.SystemIcons.WinLogo.ToImageSource();
                            break;
                        }
                    case MsgBoxMethodIcon.ApplicationIcon:
                        {
                            imageSource = IconUtilities.GetIconFromLibrary(Assembly.GetEntryAssembly().Location, false, false, false);
                            break;
                        }

                    case MsgBoxMethodIcon.Custom:
                        {
                            if (customIcon != null)
                            {
                                imageSource = customIcon;
                            }
                            break;
                        }
                    default:
                        {
                            imageSource = null;
                            break;
                        }
                }
            }
            catch { }

            this.imageIcon.Source = imageSource;

            return true;
        }

        /// <summary>
        /// Get MsgBox SilentBox Text
        /// </summary>
        /// <param name="silentBox"></param>
        /// <param name="customSilentBoxText"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        private string GetMsgBoxSilentBoxText(MsgBoxMethodSilentBox silentBox, string customSilentBoxText, string culture)
        {
            if (silentBox == MsgBoxMethodSilentBox.Custom)
            {
                if (customSilentBoxText != null || customSilentBoxText.Trim() != "")
                {
                    return customSilentBoxText.Trim();
                }

                return string.Empty;
            }

            if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
            {
                return (silentBox.ToDescriptionString());
            }
            /// <summary>
            /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
            /// OR another case
            /// </summary>
            else
            {
                return (silentBox.ToSpecialDescriptionString());
            }
        }

        /// <summary>
        /// Set MsgBox Buttons
        /// </summary>
        /// <param name="msgBoxMethodButtons"></param>
        /// <param name="msgBoxMethodButtonsText"></param>
        /// <param name="msgBoxMethodButtonsColors"></param>
        /// <returns></returns>
        private bool SetMsgBoxButtons(
            string[] msgBoxMethodButtons,
            MsgBoxMethodButtonsText[] msgBoxMethodButtonsText,
            Color[] msgBoxMethodButtonsColors)
        {
            wrapPanelButtons.Children.Clear();

            int buttonsCounter = 0;
            int customButtonsCounter = 1000;
            Button buttonNew = null;

            Style msgBoxMethodButtonstyle = this.TryFindResource("MsgBoxButtonStyle") as Style;

            foreach (string btn_Text in msgBoxMethodButtons)
            {
                if (btn_Text != null && btn_Text.Trim() != "")
                {
                    buttonNew = new Button();

                    buttonNew.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(MsgBoxMethodUnitedColors.Danger.ToDescriptionString()));

                    if (msgBoxMethodButtonsColors.Length > buttonsCounter)
                    {
                        buttonNew.Background = (SolidColorBrush)(new BrushConverter().ConvertFromString(msgBoxMethodButtonsColors[buttonsCounter].ToString()));

                        if (msgBoxMethodButtonsColors[buttonsCounter] != (Color)ColorConverter.ConvertFromString(MsgBoxMethodUnitedColors.None.ToDescriptionString()) &&
                            msgBoxMethodButtonsColors[buttonsCounter] != (Color)ColorConverter.ConvertFromString(MsgBoxMethodUnitedColors.Default.ToDescriptionString()) &&
                            msgBoxMethodButtonsColors[buttonsCounter] != (Color)ColorConverter.ConvertFromString(MsgBoxMethodUnitedColors.Warning.ToDescriptionString()) &&
                            msgBoxMethodButtonsColors[buttonsCounter] != (Color)ColorConverter.ConvertFromString(MsgBoxMethodUnitedColors.Normal.ToDescriptionString()))
                        {
                            buttonNew.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(MsgBoxMethodUnitedColors.Default.ToDescriptionString()));
                        }
                    }

                    buttonNew.Name = "button" + buttonsCounter.ToString();
                    buttonNew.TabIndex = buttonsCounter + 2;
                    buttonNew.Content = btn_Text.Trim();
                    buttonNew.Style = msgBoxMethodButtonstyle;

                    if (msgBoxMethodButtonsText.Length == msgBoxMethodButtons.Length)
                    {
                        buttonNew.Tag = EnumExtensions.GetEnumMemberIndex(msgBoxMethodButtonsText[buttonsCounter]);
                    }
                    else
                    {
                        buttonNew.Tag = ++customButtonsCounter;
                    }

                    wrapPanelButtons.Children.Add(buttonNew);
                    buttonsCounter++;
                }
            }

            return true;
        }

        /// <summary>
        /// ChangeCulture
        /// Design-FlowDirection RTL or LTR
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        private bool ChangeCulture_FlowDirection(string culture)
        {
            messageBoxWindow.FlowDirection = FlowDirection.LeftToRight;

            /// <summary>
            /// if(culture.ToUpper() == MsgBoxMethodCulture.Default.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.en_US.ToString().ToUpper())
            /// OR if(culture.ToUpper() == MsgBoxMethodCulture.Custom.ToString().ToUpper())
            /// OR another case
            /// </summary>
            FlowDirection _flowDirection = FlowDirection.LeftToRight;

            if (culture.ToUpper() == MsgBoxMethodCulture.fa_IR.ToString().ToUpper())
            {
                _flowDirection = FlowDirection.RightToLeft;
            }

            try
            {
                gridMain.FlowDirection = _flowDirection;

                textBoxCopyright.FlowDirection = FlowDirection.LeftToRight;

                textBoxMessage.Focus();
            }
            catch { return false; }

            return true;
        }

        #endregion /Construct Window

        #region Base Methods

        /// <summary>
        /// Get United Buttons Size Group
        /// Size Max Button
        /// United Size
        /// </summary>
        /// <returns>United Buttons Size</returns>
        private bool GetUnitedButtonSizeGroup()
        {
            try
            {
                Size sizeMaxButton = new Size(0, 0);

                foreach (Button btn in wrapPanelButtons.Children)
                {
                    if (btn != null)
                    {
                        if (btn.ActualWidth > sizeMaxButton.Width)
                        {
                            sizeMaxButton.Width = (double)btn.ActualWidth;
                        }
                        if (btn.ActualHeight > sizeMaxButton.Height)
                        {
                            sizeMaxButton.Height = (double)btn.ActualHeight;
                        }
                    }
                }

                foreach (Button btn in wrapPanelButtons.Children)
                {
                    if (btn != null)
                    {
                        btn.Width = sizeMaxButton.Width;
                        btn.Height = sizeMaxButton.Height;
                    }
                }

                wrapPanelButtons.Orientation = _locateButtonsOrientation;
            }
            catch { return false; }

            return true;
        }

        /// <summary>
        /// Button_Click event for sample button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MsgBoxButton_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Answer = (int)btn.Tag;
            _SilentBoxIsChecked = false;
            if (checkBoxSilentBox != null && checkBoxSilentBox.IsChecked == true)
            {
                _SilentBoxIsChecked = true;
            }
            this.Close();
        }

        /// <summary>
        /// Construct dynamic form with many cases
        /// </summary>
        /// <param name="text">message information</param>
        /// <param name="caption">window caption</param>
        /// <param name="buttons">array of possible answers</param>
        /// <param name="customButtons">custom array of possible answers</param>
        /// <param name="icon">image to be displayed as the message icon</param>
        /// <param name="customIcon">custom image to be displayed as the message icon</param>
        /// <param name="defaultButton">default answer zero-based index</param>
        /// <param name="options">options</param>
        /// <param name="buttonsColors">button colors</param>
        /// <param name="customButtonsColors">custom buttons colors</param>
        /// <param name="silentBox">silent check box; if None then check box will not be displayed</param>
        /// <param name="customSilentBoxText">custom silent check box text; if null then check box will not be displayed</param>
        /// <param name="culture">change culture and change design RTL or LTR</param>
        private void construct_Form(
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture)
        {
            /// <summary>
            /// Define Product Copyright for this component
            /// </summary>
            textBoxCopyright.Text = AssemblyAttributeAccessors.ExecutingAssemblyCopyright;

            if (text == null)
            {
                text = string.Empty;
            }
            text = text.Trim();

            if (caption == null)
            {
                caption = string.Empty;
            }
            caption = caption.Trim();

            if (customSilentBoxText == null)
            {
                customSilentBoxText = string.Empty;
            }
            customSilentBoxText = customSilentBoxText.Trim();

            if (culture == null)
            {
                culture = string.Empty;
            }
            culture = culture.Trim();

            textBoxMessage.Text = text;

            this.Title = GetMsgBoxCaption(caption, culture);

            SetMsgBoxIcon_ImageBox(icon, customIcon);

            MsgBoxMethodButtonsText[] msgBoxMethodButtonsText = GetMsgBoxMethodButtonsText(buttons);
            string[] msgBoxButtonsText = GetMsgBoxButtonsText(msgBoxMethodButtonsText, customButtons, culture);

            Color[] msgBoxButtonsColors = GetMsgBoxButtonsColors(msgBoxMethodButtonsText, buttonsColors, customButtonsColors);
            if (msgBoxButtonsColors == null) { msgBoxButtonsColors = new Color[0]; }

            checkBoxSilentBox.Content = GetMsgBoxSilentBoxText(silentBox, customSilentBoxText, culture);

            SetMsgBoxButtons(msgBoxButtonsText, msgBoxMethodButtonsText, msgBoxButtonsColors);

            ChangeCulture_FlowDirection(culture);

            int messageBoxDefaultButton = GetMsgBoxDefaultButton(defaultButton, msgBoxButtonsText.Length);
            Button foundDefaultButton =
               HelperFunctions.FindChild<Button>(wrapPanelButtons, ("button" + messageBoxDefaultButton.ToString()));

            foundDefaultButton.IsDefault = true;
            foundDefaultButton.Focus();
        }

        /// <summary>
        /// Construct dynamic dialog box with many answer cases
        /// </summary>
        /// <param name="text">message information</param>
        /// <param name="caption">window caption</param>
        /// <param name="buttons">array of possible answers</param>
        /// <param name="customButtons">custom array of possible answers</param>
        /// <param name="icon">image to be displayed as the message icon</param>
        /// <param name="customIcon">custom image to be displayed as the message icon</param>
        /// <param name="defaultButton">default answer zero-based index</param>
        /// <param name="options">options</param>
        /// <param name="buttonsColors">button colors</param>
        /// <param name="customButtonsColors">custom button colors</param>
        /// <param name="silentBox">silent check box; if None then check box will not be displayed</param>
        /// <param name="customSilentBoxText">custom silent check box text; if null then check box will not be displayed</param>
        /// <param name="culture">change culture and change design RTL or LTR</param>
        public MessageBoxWindow(
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture)
        {
            InitializeComponent();

            try
            {
                construct_Form(
                    text,
                    caption,
                    buttons, customButtons,
                    icon, customIcon,
                    defaultButton,
                    options,
                    buttonsColors, customButtonsColors,
                    silentBox, customSilentBoxText,
                    culture);
            }
            catch { }
        }

        private void messageBoxWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GetUnitedButtonSizeGroup();
                IconUtilities.RemoveWindowIcon(this);
            }
            catch { }
        }

        /// <summary>
        /// Show MessageForm as a dialog box and get user's answer
        /// </summary>
        /// <returns>zero-based index of chosen answer. Cancel = -1</returns>
        private int GetResult()
        {
            if (this.Owner == null)
            {
                //this.ShowInTaskbar = true;
                this.Focus();
                this.ShowDialog();
            }
            else
            {
                this.ShowDialog();
            }

            return Answer;
        }

        /// <summary>
        /// Show MessageForm as a dialog box and return user's answer
        /// </summary>
        /// <returns>zero-based index of chosen answer. Cancel = -1</returns>
        public int GetDialogResult()
        {
            return GetResult();
        }

        #endregion /Base Methods
    }
}
