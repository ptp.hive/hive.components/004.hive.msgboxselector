﻿using HiVE.MsgBoxSelector.Models;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;
using static HiVE.MsgBoxSelector.MsgBoxSpecialMethods;

namespace HiVE.MsgBoxSelector
{
    /// <summary>
    /// Show MessageForm with predefined features
    /// </summary>
    public static class MessageBoxSelector
    {
        #region Fields

        [ThreadStatic]
        private static HelpInfo[] helpInfoTable;

        private static object lockVariable_WindowMessage = new object();
        private static object lockVariable_WindowMain = new object();

        /// <summary>
        /// Defines how message boxes will be showed in multithreaded environment:
        /// TRUE: strongly in turn one after another even if sent from different threads;
        /// FALSE: simultaneously if sent from different threads - like .NET MessageBox does;
        /// </summary>
        private static bool ShowMessagesInTurn = true;

        /// <summary>
        /// Icon in the window header. By default it is the icon of the application.
        /// For this Error: "Argument 'picture' must be a picture that can be used as a Icon."
        /// Make sure that use of a Save As 24-bit Bitmap(*.bmp;*dib) format.
        /// Other formats cannot be used as icons, so that it all there is to it.
        /// </summary>
        private static ImageSource WindowIcon = IconUtilities.GetIconFromLibrary(Assembly.GetEntryAssembly().Location, false, false, false);

        /// <summary>
        /// Whether the message box is displayed in the Windows taskbar.
        /// </summary>
        private static bool ShowInTaskbar = true;

        /// <summary>
        /// Owner of the first next message.
        /// Not thread-safe!
        /// </summary>
        private static System.Windows.Window NextTime_Owner = null;

        #endregion /Fields

        #region HelpInfo

        /// <summary>
        /// Get Help Info
        /// </summary>
        internal static HelpInfo HelpInfo
        {
            get
            {
                if ((helpInfoTable != null) && (helpInfoTable.Length != 0))
                {
                    return helpInfoTable[helpInfoTable.Length - 1];
                }
                return null;
            }
        }

        /// <summary>
        /// Pop Help Info
        /// </summary>
        private static void PopHelpInfo()
        {
            if (helpInfoTable != null)
            {
                if (helpInfoTable.Length == 1)
                {
                    helpInfoTable = null;
                }
                else
                {
                    int length = helpInfoTable.Length - 1;
                    HelpInfo[] destinationArray = new HelpInfo[length];
                    Array.Copy(helpInfoTable, destinationArray, length);
                    helpInfoTable = destinationArray;
                }
            }
        }

        /// <summary>
        /// Push Help Info
        /// </summary>
        /// <param name="hpi"></param>
        private static void PushHelpInfo(HelpInfo hpi)
        {
            HelpInfo[] infoArray;
            int length = 0;
            if (helpInfoTable == null)
            {
                infoArray = new HelpInfo[length + 1];
            }
            else
            {
                length = helpInfoTable.Length;
                infoArray = new HelpInfo[length + 1];
                Array.Copy(helpInfoTable, infoArray, length);
            }
            infoArray[length] = hpi;
            helpInfoTable = infoArray;
        }

        #endregion /HelpInfo

        #region Base Methods

        /// <summary>
        /// PreSet WindowMessage
        /// </summary>
        /// <param name="windowMessage"></param>
        private static void PreSet(MessageBoxWindow windowMessage)
        {
            lock (lockVariable_WindowMain)
            {
                //windowMessage.Icon = WindowIcon;
                windowMessage.ShowInTaskbar = ShowInTaskbar;
                if (NextTime_Owner == null)
                {
                    //windowMessage.TopLevel = true;
                    windowMessage.Topmost = true;
                }
                else
                {
                    windowMessage.Owner = NextTime_Owner;
                }
            }
        }

        /// <summary>
        /// PostSet WindowMessage
        /// </summary>
        private static void PostSet()
        {
            lock (lockVariable_WindowMain)
            {
                NextTime_Owner = null;
            }
        }

        /// <summary>
        /// Caller isGUI Window in Main Window
        /// </summary>
        /// <returns></returns>
        private static bool Caller_isGUI_Form()
        {
            lock (lockVariable_WindowMain)
            {
                StackTrace stack = new StackTrace(true);
                foreach (StackFrame sf in stack.GetFrames())
                {
                    Type caller = sf.GetMethod().ReflectedType;
                    if (caller == typeof(System.Windows.Window))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Show Blocking Dialog
        /// </summary>
        private static void ShowBlockingDialog()
        {
            Show(
                SpecialModels.MsgBoxMethodMethodCommonMessage.ShowBlockingDialog.ToSpecialDescriptionString(),
                Assembly.GetEntryAssembly().GetName().Name,
                MsgBoxMethodButtons.OK,
                MsgBoxMethodIcon.Stop,
                MsgBoxMethodDefaultButton.Button1,
                MsgBoxMethodCulture.en_US.ToString()
                );
        }

        /// <summary>
        /// Convert Win32 To MsgDialogResult
        /// </summary>
        /// <param name="value"></param>
        /// <param name="buttons"></param>
        /// <returns></returns>
        private static MsgBoxMethodDialogResult Win32ToMsgDialogResult(int value, MsgBoxMethodButtons buttons)
        {
            if (buttons != MsgBoxMethodButtons.Custom)
            {
                return EnumExtensions.GetEnumValue<MsgBoxMethodDialogResult>(value);
            }
            else if (value > 1000 && value < 1010)
            {
                return EnumExtensions.GetEnumValue<MsgBoxMethodDialogResult>(value);
            }
            else
            {
                return MsgBoxMethodDialogResult.None;
            }
        }

        /// <summary>
        /// Construct dynamic dialog box with many answer cases
        /// Plus bool showHelp
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="text">message information</param>
        /// <param name="caption">window caption</param>
        /// <param name="buttons">array of possible answers</param>
        /// <param name="customButtons">custom array of possible answers</param>
        /// <param name="icon">image to be displayed as the message icon</param>
        /// <param name="customIcon">custom image to be displayed as the message icon</param>
        /// <param name="defaultButton">default answer zero-based index</param>
        /// <param name="options">options</param>
        /// <param name="buttonsColors">button colors</param>
        /// <param name="customButtonsColors">custom buttons colors</param>
        /// <param name="silentBox">silent check box; if None then check box will not be displayed</param>
        /// <param name="customSilentBoxText">custom silent check box text; if null then check box will not be displayed</param>
        /// <param name="culture">change culture and change design RTL or LTR</param>
        /// <param name="showHelp"></param>
        /// <returns>Msg DialogResult</returns>
        private static MsgBoxMethodDialogResult ShowCore(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool showHelp)
        {
            MsgBoxMethodDialogResult dialogResult = MsgBoxMethodDialogResult.None;

            try
            {
                /// ClientUtils.IsEnumValid MsgBoxMethodButtons
                if (!EnumExtensions.ClientUtils_IsEnumValid(buttons, (int)buttons, 0, 14))
                {
                    throw new InvalidEnumArgumentException("buttons", (int)buttons, typeof(MsgBoxMethodButtons));
                }
                /// ClientUtils.IsEnumValid MsgBoxMethodButtonsColors
                if (!EnumExtensions.ClientUtils_IsEnumValid(buttonsColors, (int)buttonsColors, 0, 2))
                {
                    throw new InvalidEnumArgumentException("buttonsColors", (int)buttonsColors, typeof(MsgBoxMethodButtonsColors));
                }
                /// WindowsFormsUtils.EnumValidator.IsEnumWithinShiftedRange MsgBoxMethodIcon
                if (!EnumExtensions.ClientUtils_IsEnumValid(icon, (int)icon, 0, 12))
                {
                    throw new InvalidEnumArgumentException("icon", (int)icon, typeof(MsgBoxMethodIcon));
                }
                /// ClientUtils.IsEnumValid MsgBoxMethodDefaultButton
                if (!EnumExtensions.ClientUtils_IsEnumValid(silentBox, (int)defaultButton, 0, 9))
                {
                    throw new InvalidEnumArgumentException("defaultButton", (int)defaultButton, typeof(MsgBoxMethodDefaultButton));
                }
                /// ClientUtils.IsEnumValid MsgBoxMethodSilentBox
                if (!EnumExtensions.ClientUtils_IsEnumValid(silentBox, (int)silentBox, 0, 4))
                {
                    throw new InvalidEnumArgumentException("silentBox", (int)silentBox, typeof(MsgBoxMethodSilentBox));
                }

                //if (!System.Windows.Forms.SystemInformation.UserInteractive &&
                //    ((options & (MsgBoxOptions.ServiceNotification | MsgBoxOptions.DefaultDesktopOnly)) == 0))
                //{
                //    throw new InvalidOperationException(SR.GetString("CantShowModalOnNonInteractive"));
                //}
                if ((owner != null) && ((options & (MsgBoxMethodOptions.ServiceNotification | MsgBoxMethodOptions.DefaultDesktopOnly)) != 0))
                {
                    throw new ArgumentException(SR.GetString("CantShowMBServiceWithOwner"), "options");
                }
                if (showHelp && ((options & (MsgBoxMethodOptions.ServiceNotification | MsgBoxMethodOptions.DefaultDesktopOnly)) != 0))
                {
                    throw new ArgumentException(SR.GetString("CantShowMBServiceWithHelp"), "options");
                }
                if ((options & ~(MsgBoxMethodOptions.RtlReading | MsgBoxMethodOptions.RightAlign)) != 0)
                {
                    /// IntSecurity.UnmanagedCode.Demand();
                    //IntSecurity.UnmanagedCode.Demand();
                }

                try
                {
                    if (ShowMessagesInTurn)
                    {
                        if (Caller_isGUI_Form())
                        {
                            while (!System.Threading.Monitor.TryEnter(lockVariable_WindowMessage))
                            { ShowBlockingDialog(); }
                            //while (!System.Threading.Monitor.TryEnter(lockVariable_MessageForm))
                            //{ Application.DoEvents(); }
                        }
                        else
                        {
                            System.Threading.Monitor.Enter(lockVariable_WindowMessage);
                        }
                    }

                    MessageBoxWindow _messageBoxWindow =
                        new MessageBoxWindow(
                            text,
                            caption,
                            buttons, customButtons,
                            icon, customIcon,
                            defaultButton,
                            options,
                            buttonsColors, customButtonsColors,
                            silentBox, customSilentBoxText,
                            culture);

                    PreSet(_messageBoxWindow);
                    int result = _messageBoxWindow.GetDialogResult();
                    PostSet();

                    dialogResult = Win32ToMsgDialogResult(result, buttons);
                }
                finally
                {
                    if (ShowMessagesInTurn)
                        System.Threading.Monitor.Exit(lockVariable_WindowMessage);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show(
                    SpecialModels.MsgBoxMethodMethodCommonMessage.UnexpectedError.ToSpecialDescriptionString(),
                    Assembly.GetExecutingAssembly().GetName().Name,
                    System.Windows.MessageBoxButton.OK,
                    System.Windows.MessageBoxImage.Error
                    );
            }

            return dialogResult;
        }

        /// <summary>
        /// Construct dynamic dialog box with many answer cases
        /// Plus HelpInfo hpi
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="text">message information</param>
        /// <param name="caption">window caption</param>
        /// <param name="buttons">array of possible answers</param>
        /// <param name="customButtons">custom array of possible answers</param>
        /// <param name="icon">image to be displayed as the message icon</param>
        /// <param name="customIcon">custom image to be displayed as the message icon</param>
        /// <param name="defaultButton">default answer zero-based index</param>
        /// <param name="options">options</param>
        /// <param name="buttonsColors">button colors</param>
        /// <param name="customButtonsColors">custom buttons colors</param>
        /// <param name="silentBox">silent check box; if None then check box will not be displayed</param>
        /// <param name="customSilentBoxText">custom silent check box text; if null then check box will not be displayed</param>
        /// <param name="culture">change culture and change design RTL or LTR</param>
        /// <param name="hpi"></param>
        /// <returns>Msg DialogResult</returns>
        private static MsgBoxMethodDialogResult ShowCore(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            HelpInfo hpi)
        {
            MsgBoxMethodDialogResult dialogResult = MsgBoxMethodDialogResult.None;
            try
            {
                PushHelpInfo(hpi);
                dialogResult =
                    ShowCore(
                        owner,
                        text,
                        caption,
                        buttons, customButtons,
                        icon, customIcon,
                        defaultButton,
                        options,
                        buttonsColors, customButtonsColors,
                        silentBox, customSilentBoxText,
                        culture,
                        true);
            }
            finally
            {
                PopHelpInfo();
            }
            return dialogResult;
        }

        #endregion /Base Methods

        #region Return Show Core Parameters

        /// <summary>
        /// ShowCore Parameters _ plus_bool_showHelp
        /// </summary>
        /// <returns></returns>
        private static string ShowCoreParameters_plus_bool_showHelp()
        {
            string ShowCoreParameters_plus_bool_showHelp = string.Empty;

            ShowCoreParameters_plus_bool_showHelp =
                string.Format(
                    "{0}," + "\n" +
                    "{1}," + "\n" +
                    "{2}," + "\n" +
                    "{3}, {4}," + "\n" +
                    "{5}, {6}," + "\n" +
                    "{7}," + "\n" +
                    "{8}," + "\n" +
                    "{9}, {10}," + "\n" +
                    "{11}, {12}," + "\n" +
                    "{13}," + "\n" +
                    "{14}",

                    "Construct dynamic dialog box with many answer cases" + "\n" +
                    "Plus bool showHelp",

                    "IWin32Window owner",
                    "string text",
                    "string caption",
                    "MsgBoxButtons buttons", "string[] customButtons",
                    "MsgBoxIcon icon", "System.Windows.Media.Imaging.BitmapImage customIcon",
                    "MsgBoxDefaultButton defaultButton",
                    "MsgBoxOptions options",
                    "MsgBoxButtonsColors buttonsColors", "Color[] customButtonsColors",
                    "MsgBoxSilentBox silentBox", "string customSilentBoxText",
                    "string culture",
                    "bool showHelp"
                    );

            return ShowCoreParameters_plus_bool_showHelp;
        }

        /// <summary>
        /// ShowCore Parameters _ plus_bool_showHelp _ Summary
        /// </summary>
        /// <returns></returns>
        private static string ShowCoreParameters_plus_bool_showHelp_Summary()
        {
            string ShowCoreParameters_plus_bool_showHelp_Summary = string.Empty;

            ShowCoreParameters_plus_bool_showHelp_Summary =
                string.Format(
                    "{0}," + "\n" +
                    "{1}," + "\n" +
                    "{2}," + "\n" +
                    "{3}," + "\n" + "{4}," + "\n" +
                    "{5}," + "\n" + "{6}," + "\n" +
                    "{7}," + "\n" +
                    "{8}," + "\n" +
                    "{9}," + "\n" + "{10}," + "\n" + "\n" +
                    "{11}," + "\n" + "{12}," + "\n" +
                    "{13}," + "\n" +
                    "{14}," + "\n" +
                    "{15}",

                    "Construct dynamic dialog box with many answer cases" + "\n" +
                    "Plus bool showHelp",

                    "<param name='IWin32Window owner'",
                    "<param name='string text'>message information</param>",
                    "<param name='string caption'",

                    "<param name='MsgBoxButtons buttons'",
                    "<param name='string[] customButtons'",

                    "<param name='MsgBoxIcon icon'",
                    "<param name='System.Windows.Media.Imaging.BitmapImage customIcon'",

                    "<param name='MsgBoxDefaultButton defaultButton'",
                    "<param name='MsgBoxOptions options'",

                    "<param name='MsgBoxButtonsColors buttonsColors'",
                    "<param name='Color[] customButtonsColors'",

                    "<param name='MsgBoxSilentBox silentBox'",
                    "<param name='string customSilentBoxText'",

                    "<param name='string culture'",
                    "<param name='bool showHelp'",
                    "<returns>Msg DialogResult</returns>"
                    );

            return ShowCoreParameters_plus_bool_showHelp_Summary;
        }

        /// <summary>
        /// ShowCore Parameters _ plus_HelpInfo_hpi
        /// </summary>
        /// <returns></returns>
        private static string ShowCoreParameters_plus_HelpInfo_hpi()
        {
            string ShowCoreParameters_plus_HelpInfo_hpi = string.Empty;

            ShowCoreParameters_plus_HelpInfo_hpi =
                string.Format(
                    "{0}," + "\n" +
                    "{1}," + "\n" +
                    "{2}," + "\n" +
                    "{3}, {4}," + "\n" +
                    "{5}, {6}," + "\n" +
                    "{7}," + "\n" +
                    "{8}," + "\n" +
                    "{9}, {10}," + "\n" +
                    "{11}, {12}," + "\n" +
                    "{13}," + "\n" +
                    "{14}",

                    "Construct dynamic dialog box with many answer cases" + "\n" +
                    "Plus bool showHelp",

                    "IWin32Window owner",
                    "string text",
                    "string caption",
                    "MsgBoxButtons buttons", "string[] customButtons",
                    "MsgBoxIcon icon", "System.Windows.Media.Imaging.BitmapImage customIcon",
                    "MsgBoxDefaultButton defaultButton",
                    "MsgBoxOptions options",
                    "MsgBoxButtonsColors buttonsColors", "Color[] customButtonsColors",
                    "MsgBoxSilentBox silentBox", "string customSilentBoxText",
                    "string culture",
                    "bool showHelp"
                    );

            return ShowCoreParameters_plus_HelpInfo_hpi;
        }

        /// <summary>
        /// ShowCore Parameters _ plus_HelpInfo_hpi _ Summary
        /// </summary>
        /// <returns></returns>
        private static string ShowCoreParameters_plus_HelpInfo_hpi_Summary()
        {
            string ShowCoreParameters_plus_HelpInfo_hpi_Summary = string.Empty;

            ShowCoreParameters_plus_HelpInfo_hpi_Summary =
                string.Format(
                    "{0}," + "\n" +
                    "{1}," + "\n" +
                    "{2}," + "\n" +
                    "{3}," + "\n" + "{4}," + "\n" +
                    "{5}," + "\n" + "{6}," + "\n" +
                    "{7}," + "\n" +
                    "{8}," + "\n" +
                    "{9}," + "\n" + "{10}," + "\n" + "\n" +
                    "{11}," + "\n" + "{12}," + "\n" +
                    "{13}," + "\n" +
                    "{14}," + "\n" +
                    "{15}",

                    "Construct dynamic dialog box with many answer cases" + "\n" +
                    "Plus bool showHelp",

                    "<param name='IWin32Window owner'",
                    "<param name='string text'>message information</param>",
                    "<param name='string caption'",

                    "<param name='MsgBoxButtons buttons'",
                    "<param name='string[] customButtons'",

                    "<param name='MsgBoxIcon icon'",
                    "<param name='System.Windows.Media.Imaging.BitmapImage customIcon'",

                    "<param name='MsgBoxDefaultButton defaultButton'",
                    "<param name='MsgBoxOptions options'",

                    "<param name='MsgBoxButtonsColors buttonsColors'",
                    "<param name='Color[] customButtonsColors'",

                    "<param name='MsgBoxSilentBox silentBox'",
                    "<param name='string customSilentBoxText'",

                    "<param name='string culture'",
                    "<param name='HelpInfo hpi'",
                    "<returns>Msg DialogResult</returns>"
                    );

            return ShowCoreParameters_plus_HelpInfo_hpi_Summary;
        }

        #endregion /Return Show Core Parameters

        #region Get Show Core Parameters

        /// <summary>
        /// Get ShowCore Parameters _ plus_bool_showHelp
        /// </summary>
        public static string GetShowCoreParameters_plus_bool_showHelp
        {
            get { return ShowCoreParameters_plus_bool_showHelp(); }
        }

        /// <summary>
        /// Get ShowCore Parameters _ plus_bool_showHelp _ Summary
        /// </summary>
        public static string GetShowCoreParameters_plus_bool_showHelp_Summary
        {
            get { return ShowCoreParameters_plus_bool_showHelp_Summary(); }
        }

        /// <summary>
        /// Get ShowCore Parameters _ plus_HelpInfo_hpi
        /// </summary>
        public static string GetShowCoreParameters_plus_HelpInfo_hpi
        {
            get { return ShowCoreParameters_plus_HelpInfo_hpi(); }
        }

        /// <summary>
        /// Get ShowCore Parameters _ plus_HelpInfo_hpi _ Summary
        /// </summary>
        public static string GetShowCoreParameters_plus_HelpInfo_hpi_Summary
        {
            get { return ShowCoreParameters_plus_HelpInfo_hpi_Summary(); }
        }

        #endregion /Get Show Core Parameters

        #region Get Construct dynamic dialog box with many cases of bool showHelp

        public static MsgBoxMethodDialogResult Show(
            string text,
            string culture) =>
            ShowCore(
                null,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string culture) =>
            ShowCore(
                owner,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                string.Empty,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);


        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtonsColors buttonsColors, Color customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.OK, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, new Color[1] { customButtonsColors },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.None, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                0,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0, buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                MsgBoxMethodDefaultButton.Button1,
                0,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              icon, null,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              icon, null,
              defaultButton,
              0,
              buttonsColors, customButtonsColors,
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              icon, null,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              icon, null,
              defaultButton,
              0,
              buttonsColors, customButtonsColors,
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              icon, null,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              icon, null,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Custom, customButtonsColors,
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              icon, null,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              icon, null,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Custom, customButtonsColors,
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              buttonsColors, customButtonsColors,
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              buttons, new string[1] { string.Empty },
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              buttonsColors, customButtonsColors,
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Custom, customButtonsColors,
              MsgBoxMethodSilentBox.None, string.Empty,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
          ShowCore(
              owner,
              text,
              caption,
              MsgBoxMethodButtons.Custom, customButtons,
              MsgBoxMethodIcon.Custom, customIcon,
              defaultButton,
              0,
              MsgBoxMethodButtonsColors.Custom, customButtonsColors,
              silentBox, customSilentBoxText,
              culture,
              false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture) =>
            ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                false);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            bool displayHelpButton) =>
            ShowCore(
                null,
                text,
                caption,
                buttons, customButtons,
                icon, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                displayHelpButton);

        #endregion /Get Construct dynamic dialog box with many cases of bool showHelp

        #region Get Construct dynamic dialog box with many cases of HelpInfo hpi

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon, Image customIcon_null,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon, Image customIcon_null,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon, Image customIcon_null,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon, Image customIcon_null,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options, buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                null,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, string keyword)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, keyword);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options, buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                icon, null,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            MsgBoxMethodIcon icon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                icon, null,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Default, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, new string[1] { string.Empty },
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                MsgBoxMethodSilentBox.None, string.Empty,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.None, new Color[1] { Color.FromArgb(0, 0, 0, 0) },
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            string[] customButtons,
            System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                MsgBoxMethodButtons.Custom, customButtons,
                MsgBoxMethodIcon.Custom, customIcon,
                defaultButton,
                options,
                MsgBoxMethodButtonsColors.Custom, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        public static MsgBoxMethodDialogResult Show(
            MsgBoxMethodIWin32Window owner,
            string text,
            string caption,
            MsgBoxMethodButtons buttons, string[] customButtons,
            MsgBoxMethodIcon icon, System.Windows.Media.Imaging.BitmapImage customIcon,
            MsgBoxMethodDefaultButton defaultButton,
            MsgBoxMethodOptions options,
            MsgBoxMethodButtonsColors buttonsColors, Color[] customButtonsColors,
            MsgBoxMethodSilentBox silentBox, string customSilentBoxText,
            string culture,
            string helpFilePath, MsgBoxMethodHelpNavigator navigator, object param)
        {
            HelpInfo hpi = new HelpInfo(helpFilePath, navigator, param);
            return ShowCore(
                owner,
                text,
                caption,
                buttons, customButtons,
                icon, customIcon,
                defaultButton,
                options,
                buttonsColors, customButtonsColors,
                silentBox, customSilentBoxText,
                culture,
                hpi);
        }

        #endregion /Get Construct dynamic dialog box with many cases of HelpInfo hpi
    }
}
