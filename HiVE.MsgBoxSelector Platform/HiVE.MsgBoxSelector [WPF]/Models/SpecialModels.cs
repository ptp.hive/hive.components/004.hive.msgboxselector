﻿using System.ComponentModel;

namespace HiVE.MsgBoxSelector.Models
{
    internal static class SpecialModels
    {
        public enum MsgBoxMethodMethodCommonMessage
        {
            [SpecialDescription("Logic Error!")]
            [Description("خطای منطقی.")]
            LogicError,
            [SpecialDescription("An unexpected error!")]
            [Description("یک خطای غیر منتظره!")]
            UnexpectedError,

            [SpecialDescription("Please close all message boxes opened by this application, before using this window!")]
            [Description("لطفا تمام جعبه پیام باز شده توسط این نرم افزار را ببندید، قبل از استفاده از این پنجره!")]
            ShowBlockingDialog,
            [SpecialDescription("Could not extract default icon for MessageBox Window!")]
            [Description("نماد پیش فرض برای پیام جعبه پنجره استخراج نشده!")]
            CouldNotExtractIcon
        }
    }
}
