﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.MsgBoxSelector.Models
{
    internal class Converter_StringEmptyToVisibility : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Collapsed;

            try
            {
                if (value != null)
                {
                    if (value.ToString().Trim() != "")
                    {
                        returnValue = Visibility.Visible;
                    }
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Visibility back to StringEmpty");
        }
    }
}
