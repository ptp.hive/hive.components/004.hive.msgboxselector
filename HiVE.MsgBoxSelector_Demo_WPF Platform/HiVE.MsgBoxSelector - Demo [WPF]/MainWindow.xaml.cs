﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using System;
using System.Windows;
using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.MsgBoxSelector_Demo_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    internal partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_MsgBoxSelector_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsWindow;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsWindow.MainWindow).ToString();

        private static TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsWindow_MainWindow TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        public BoxBasicWindowDetails BasicWindowDetailsItem { get; set; }

        private void buttonMainMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch { }
        }

        private static BoxBasicWindowDetails DisplayBasicWindowDetails()
        {
            BoxBasicWindowDetails oBoxBasicWindowDetails =
                new BoxBasicWindowDetails(
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    FlowDirection.LeftToRight,
                    MethodCulture.Default);

            try
            {
                oBoxBasicWindowDetails.MainMenuContent = BasicMethods.BasicMainMenuContent;
                oBoxBasicWindowDetails.MainMenuContent = string.Empty;

                oBoxBasicWindowDetails.AssemblyProductVersionMajor =
                    string.Format(
                        "{0} v{1}",
                        BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                        BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor
                        );

                oBoxBasicWindowDetails.AssemblyCopyright = BasicAssemblyAttributeAccessors.EntryAssemblyCopyright;

                oBoxBasicWindowDetails.ProductIsTrialMode = MethodProductTypeTrialMode.Demo.ToSpecialDescriptionString();

                oBoxBasicWindowDetails.BasicFlowDirection = BasicMethods.BasicFlowDirection;
                oBoxBasicWindowDetails.BasicCulture = BasicMethods.BasicCulture;
            }
            catch { }

            return oBoxBasicWindowDetails;
        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsWindow_MainWindow.ThisWindow_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                BasicWindowDetailsItem = DisplayBasicWindowDetails();
                mainWindow.DataContext = BasicWindowDetailsItem;
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingWindowError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
