﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Demo [WPF] for HiVE.MsgBoxSelector.")]
[assembly: AssemblyDescription("Some Example for several construct message box methods!")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HiVE Productions")]
[assembly: AssemblyProduct("HiVE.MsgBoxSelector - Demo [WPF]")]
[assembly: AssemblyCopyright("Copyright ©  2016-2017 HiVE Productions Inc. All Rights Reserved.")]
[assembly: AssemblyTrademark("HiVE")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

//In order to begin building localizable applications, set 
//<UICulture>CultureYouAreCodingWith</UICulture> in your .csproj file
//inside a <PropertyGroup>.  For example, if you are using US english
//in your source files, set the <UICulture> to en-US.  Then uncomment
//the NeutralResourceLanguage attribute below.  Update the "en-US" in
//the line below to match the UICulture setting in the project file.

//[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.Satellite)]


[assembly: ThemeInfo(
    ResourceDictionaryLocation.None, //where theme specific resource dictionaries are located
                                     //(used if a resource is not found in the page, 
                                     // or application resource dictionaries)
    ResourceDictionaryLocation.SourceAssembly //where the generic resource dictionary is located
                                              //(used if a resource is not found in the page, 
                                              // app, or any theme specific resource dictionaries)
)]


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.4.1704.2915")]
[assembly: AssemblyFileVersion("3.4.1704.2915")]
[assembly: Guid("8dd64763-8e76-4e8c-9d3e-e8ed75cea004")]
[assembly: NeutralResourcesLanguage("en")]

