﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using System;
using System.Diagnostics;
using System.Windows.Controls;
using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.MsgBoxSelector_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    internal partial class About : UserControl
    {
        public About()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_MsgBoxSelector_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl.About).ToString();

        private TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_About TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                e.Handled = true;
            }
            catch { }
        }

        private void LoadAssemblyAttributeAccessors()
        {
            try
            {
                textBlockProductVersionMajor.Text = string.Format(
                    "{0} v{1}",
                    BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                    BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor
                    );
                textBlockProduct.Text = BasicAssemblyAttributeAccessors.EntryAssemblyProduct;
                textBlockVersion.Text = string.Format(
                    "Version {0}",
                    BasicAssemblyAttributeAccessors.EntryAssemblyVersion
                    );
                textBlockTitle.Text = BasicAssemblyAttributeAccessors.EntryAssemblyTitle;
                textBlockCopyright.Text = BasicAssemblyAttributeAccessors.EntryAssemblyCopyright;
                textBlockCompany.Text = BasicAssemblyAttributeAccessors.EntryAssemblyCompany;

                Uri uriHyperlinkCompanyWebSite = new Uri(MethodCompanyInformation.CompanyWebSite.ToSpecialDescriptionString(), UriKind.Absolute);
                hyperlinkCompanyWebSite.NavigateUri = uriHyperlinkCompanyWebSite;
                textBlockCompanyWebSite.Text = MethodCompanyInformation.CompanyWebSite.ToDescriptionString();

                Uri uriHyperlinkCompanyEmail = new Uri(MethodCompanyInformation.CompanyEmail.ToSpecialDescriptionString(), UriKind.Absolute);
                hyperlinkCompanyEmail.NavigateUri = uriHyperlinkCompanyEmail;
                textBlockCompanyEmail.Text = MethodCompanyInformation.CompanyEmail.ToDescriptionString();

                textBlockProductTypeLicensed.Text = MethodProductTypeLicensed.PersonalNonCommercial.ToSpecialDescriptionString();
                textBoxDescription.Text = BasicAssemblyAttributeAccessors.EntryAssemblyDescription;
            }
            catch { }
        }

        private void about_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_About.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                LoadAssemblyAttributeAccessors();
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
