﻿using HiVE.BasicModels.isClass;
using System.Windows.Controls;

namespace HiVE.MsgBoxSelector_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for ColorListBox.xaml
    /// </summary>
    internal partial class ColorListBox : UserControl
    {
        public ColorListBox()
        {
            Colors = new ColorList();
            InitializeComponent();
        }

        public ColorList Colors { get; set; }
    }
}
