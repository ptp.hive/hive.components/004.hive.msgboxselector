﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using HiVE.MsgBoxSelector;
using HiVE.MsgBoxSelector_Demo_WPF.Models;
using System;
using System.Windows;
using System.Windows.Controls;
using static HiVE.BasicModels.isClass.TryCatch;
using static HiVE.MsgBoxSelector.MsgBoxSpecialMethods;

namespace HiVE.MsgBoxSelector_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for MsgBoxSelector_Default.xaml
    /// </summary>
    internal partial class MsgBoxSelector_Default : UserControl
    {
        public MsgBoxSelector_Default()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_MsgBoxSelector_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl.MsgBoxSelector_Default).ToString();

        private TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_MsgBoxSelector_Default TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        #region Feilds

        private static string _caption = AssemblyAttributeAccessors.ExecutingAssemblyProduct;

        private static string _message_En = TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString();
        private static string _message_Fa = TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString();

        private static string _messagePlus_En =
            TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n";

        private static string _messagePlus_Fa =
            TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n";

        private static string _longMessage_En =
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En;

        private static string _longMessage_Fa =
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa;

        #endregion

        #region StackPanel Sample MsgBoxSelector - Default

        private void ButtonSample01_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _message_En,
                    MsgBoxMethodCulture.Default.ToString()
                    );
            }
            catch { }
        }

        private void ButtonSample02_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _message_Fa,
                    MsgBoxMethodCaption.Information.ToString(),
                    MsgBoxMethodButtons.OKCancel,
                    MsgBoxMethodIcon.Shield,
                    MsgBoxMethodDefaultButton.Button1,
                    MsgBoxMethodCulture.fa_IR.ToString()
                    );
            }
            catch { }
        }

        private void ButtonSample03_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _messagePlus_En,
                    MsgBoxMethodCaption.Information.ToString(),
                    MsgBoxMethodButtons.OKCancel,
                    MsgBoxMethodIcon.Shield,
                    MsgBoxMethodDefaultButton.Button1,
                    MsgBoxMethodCulture.en_US.ToString()
                    );
            }
            catch { }
        }

        private void ButtonSample04_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    "First Message _ Text",
                    "Caption",
                    MsgBoxMethodButtons.AbortRetryIgnore,
                    MsgBoxMethodIcon.Stop,
                    MsgBoxMethodCulture.en_US.ToString()).ToString();
            }
            catch { }
        }

        private void ButtonSample05_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _messagePlus_Fa,
                    _caption,
                    MsgBoxMethodButtons.OKCancel,
                    MsgBoxMethodCulture.fa_IR.ToString());
            }
            catch { }
        }

        private void ButtonSample06_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _messagePlus_En,
                    _caption,
                    MsgBoxMethodButtons.ReconnectExit,
                    MsgBoxMethodIcon.ApplicationIcon,
                    MsgBoxMethodCulture.en_US.ToString());
            }
            catch { }
        }

        private void ButtonSample07_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En
                    , MsgBoxMethodCulture.Default.ToString());
            }
            catch { }
        }

        #endregion

        private void msgBoxSelector_Default_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_MsgBoxSelector_Default.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
