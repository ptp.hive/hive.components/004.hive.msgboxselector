﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using HiVE.MsgBoxSelector;
using Microsoft.Win32;
using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static HiVE.BasicModels.isClass.TryCatch;
using static HiVE.MsgBoxSelector.MsgBoxSpecialMethods;

namespace HiVE.MsgBoxSelector_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for MsgBoxSelector_Construct.xaml
    /// </summary>
    internal partial class MsgBoxSelector_Construct : UserControl
    {
        public MsgBoxSelector_Construct()
        {
            Colors = new ColorList();
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_MsgBoxSelector_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl.MsgBoxSelector_Construct).ToString();

        private TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_MsgBoxSelector_Construct TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        #region Feilds

        private static string GetDefaultInputPath =
            System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        public ColorList Colors { get; set; }

        private BoxColorDetails colorDetailsItem { get; set; }

        private ComboBoxItem comboBoxItemSelected { get; set; }

        private string stringNull = "NULL";
        private string stringCustom = "Custom";

        #endregion Feilds

        #region comboBoxMsgBoxMethodCaption

        private void comboBoxMsgBoxMethodCaption_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodCaption.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodCaption.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodCaption.
                var msgBoxMethodCaption = EnumExtensions.GetEnumValues(new MsgBoxMethodCaption());
                /// Add each one to this collection:
                foreach (var caption in msgBoxMethodCaption)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = caption.ToString();
                    comboBoxMsgBoxMethodCaption.Items.Add(comboBoxItemAdded);
                }

                comboBoxItemAdded = new ComboBoxItem();
                comboBoxItemAdded.Content = stringCustom;
                comboBoxMsgBoxMethodCaption.Items.Add(comboBoxItemAdded);

                if (comboBoxMsgBoxMethodCaption.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodCaption.SelectedItem =
                        comboBoxMsgBoxMethodCaption.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        private void comboBoxMsgBoxMethodCaption_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodCaption.SelectedValue;

                if (comboBoxItemSelected != null)
                {
                    if (comboBoxItemSelected.Content.ToString().ToUpper() == stringCustom.ToUpper())
                    {
                        checkBoxCustomCaption.IsChecked = true;
                    }
                }
            }
            catch { }
        }

        private void checkBoxCustomCaption_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodCaption.Items.Count > 0)
                {
                    comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodCaption.Items.GetItemAt(comboBoxMsgBoxMethodCaption.Items.Count - 1);
                    comboBoxItemSelected.IsSelected = true;
                }
            }
            catch { }
        }

        private void checkBoxCustomCaption_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodCaption.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodCaption.SelectedItem =
                        comboBoxMsgBoxMethodCaption.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        #endregion

        #region comboBoxMsgBoxMethodButtons

        private void comboBoxMsgBoxMethodButtons_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodButtons.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodButtons.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodButtons.
                var msgBoxMethodButtons = EnumExtensions.GetEnumValues(new MsgBoxMethodButtons());
                /// Add each one to this collection:
                foreach (var button in msgBoxMethodButtons)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = button.ToString();
                    comboBoxMsgBoxMethodButtons.Items.Add(comboBoxItemAdded);
                }

                if (comboBoxMsgBoxMethodButtons.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodButtons.SelectedItem =
                        comboBoxMsgBoxMethodButtons.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        private void comboBoxMsgBoxMethodButtons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodButtons.SelectedValue;

                if (comboBoxItemSelected != null)
                {
                    if (comboBoxItemSelected.Content.ToString().ToUpper() == stringCustom.ToUpper())
                    {
                        checkBoxCustomButtons.IsChecked = true;
                    }
                }
            }
            catch { }
        }

        private void checkBoxCustomButtons_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodButtons.Items.Count > 0)
                {
                    comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodButtons.Items.GetItemAt(comboBoxMsgBoxMethodButtons.Items.Count - 1);
                    comboBoxItemSelected.IsSelected = true;
                }
            }
            catch { }
        }

        private void checkBoxCustomButtons_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodButtons.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodButtons.SelectedItem =
                        comboBoxMsgBoxMethodButtons.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        #endregion

        #region comboBoxMsgBoxMethodIcon

        private void comboBoxMsgBoxMethodIcon_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodIcon.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodIcon.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodIcon.
                var msgBoxMethodIcon = EnumExtensions.GetEnumValues(new MsgBoxMethodIcon());
                /// Add each one to this collection:
                foreach (var button in msgBoxMethodIcon)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = button.ToString();
                    comboBoxMsgBoxMethodIcon.Items.Add(comboBoxItemAdded);
                }

                if (comboBoxMsgBoxMethodIcon.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodIcon.SelectedItem =
                        comboBoxMsgBoxMethodIcon.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        private void comboBoxMsgBoxMethodIcon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodIcon.SelectedValue;

                if (comboBoxItemSelected != null)
                {
                    if (comboBoxItemSelected.Content.ToString().ToUpper() == stringCustom.ToUpper())
                    {
                        checkBoxCustomIcon.IsChecked = true;
                    }
                }
            }
            catch { }
        }

        private void checkBoxCustomIcon_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodIcon.Items.Count > 0)
                {
                    comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodIcon.Items.GetItemAt(comboBoxMsgBoxMethodIcon.Items.Count - 1);
                    comboBoxItemSelected.IsSelected = true;
                }
            }
            catch { }
        }

        private void checkBoxCustomIcon_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodIcon.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodIcon.SelectedItem =
                        comboBoxMsgBoxMethodIcon.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        private void buttonSelectCustomIcon_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_MsgBoxSelector_Construct.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                OpenFileDialog opfLoadFile = new OpenFileDialog();

                opfLoadFile.InitialDirectory = GetDefaultInputPath;
                try
                {
                    if (textBlockCustomIconPath.Text != null && textBlockCustomIconPath.Text != "")
                    {
                        /// Checking Exists Last Input Path and File
                        if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(textBlockCustomIconPath.Text)))
                        {
                            opfLoadFile.InitialDirectory =
                                System.IO.Directory.CreateDirectory(
                                    System.IO.Path.GetDirectoryName(textBlockCustomIconPath.Text))
                                    .ToString();
                        }
                    }
                }
                catch (Exception exc)
                {
                    #region Update boxTryCatch

                    boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction2;
                    boxTryCatch.MessageError = exc.Message;
                    boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingInputFilePathError.ToDescriptionString();
                    boxTryCatch.IsHaveError = true;

                    #endregion
                }

                opfLoadFile.Title = "Please select an image file to Inputed.";
                opfLoadFile.Multiselect = false;

                opfLoadFile.Filter = "Image Files (*.jpeg; *.png; *.ico)|*.jpeg; *.png; *.ico";
                opfLoadFile.ShowDialog();
                if (opfLoadFile.SafeFileName != null && opfLoadFile.SafeFileName != "")
                {
                    textBlockCustomIconPath.Text = opfLoadFile.FileName;
                    try
                    {
                        imageCustomIcon.Source = new BitmapImage(new Uri(textBlockCustomIconPath.Text));
                    }
                    catch
                    {
                        textBlockCustomIconPath.Text = string.Empty;
                    }
                }
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingInputFilePathError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }

        #endregion

        #region comboBoxMsgBoxMethodDefaultButton

        private void comboBoxMsgBoxMethodDefaultButton_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodDefaultButton.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodDefaultButton.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodDefaultButton.
                var msgBoxMethodDefaultButton = EnumExtensions.GetEnumValues(new MsgBoxMethodDefaultButton());
                /// Add each one to this collection:
                foreach (var button in msgBoxMethodDefaultButton)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = button.ToString();
                    comboBoxMsgBoxMethodDefaultButton.Items.Add(comboBoxItemAdded);
                }

                if (comboBoxMsgBoxMethodDefaultButton.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodDefaultButton.SelectedItem =
                        comboBoxMsgBoxMethodDefaultButton.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        #endregion

        #region comboBoxMsgBoxMethodOptions

        private void comboBoxMsgBoxMethodOptions_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodOptions.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodOptions.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodOptions.
                var msgBoxMethodOptions = EnumExtensions.GetEnumValues(new MsgBoxMethodOptions());
                /// Add each one to this collection:
                foreach (var button in msgBoxMethodOptions)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = button.ToString();
                    comboBoxMsgBoxMethodOptions.Items.Add(comboBoxItemAdded);
                }

                if (comboBoxMsgBoxMethodOptions.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodOptions.SelectedItem =
                        comboBoxMsgBoxMethodOptions.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        #endregion

        #region comboBoxMsgBoxMethodButtonsColors

        private void comboBoxMsgBoxMethodButtonsColors_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodButtonsColors.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodButtonsColors.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodButtonsColors.
                var msgBoxMethodButtonsColors = EnumExtensions.GetEnumValues(new MsgBoxMethodButtonsColors());
                /// Add each one to this collection:
                foreach (var button in msgBoxMethodButtonsColors)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = button.ToString();
                    comboBoxMsgBoxMethodButtonsColors.Items.Add(comboBoxItemAdded);
                }

                if (comboBoxMsgBoxMethodButtonsColors.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodButtonsColors.SelectedItem =
                        comboBoxMsgBoxMethodButtonsColors.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        private void comboBoxMsgBoxMethodButtonsColors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodButtonsColors.SelectedValue;

                if (comboBoxItemSelected != null)
                {
                    if (comboBoxItemSelected.Content.ToString().ToUpper() == stringCustom.ToUpper())
                    {
                        checkBoxCustomButtonsColors.IsChecked = true;
                    }
                }
            }
            catch { }
        }

        private void checkBoxCustomButtonsColors_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodButtonsColors.Items.Count > 0)
                {
                    comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodButtonsColors.Items.GetItemAt(comboBoxMsgBoxMethodButtonsColors.Items.Count - 1);
                    comboBoxItemSelected.IsSelected = true;
                }
            }
            catch { }
        }

        private void checkBoxCustomButtonsColors_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodButtonsColors.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodButtonsColors.SelectedItem =
                        comboBoxMsgBoxMethodButtonsColors.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        private void buttonAddColors_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if ((BoxColorDetails)listBoxColorsList.SelectedValue != null)
                {
                    listBoxColorsListSelected.Items.Add((BoxColorDetails)listBoxColorsList.SelectedValue);
                }
            }
            catch { }
        }

        private void buttonAddCustomColor_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (textBoxCustomColorA.Text.Trim() != "" &&
                    textBoxCustomColorR.Text.Trim() != "" &&
                    textBoxCustomColorG.Text.Trim() != "" &&
                    textBoxCustomColorB.Text.Trim() != "")
                {
                    colorDetailsItem = new BoxColorDetails();
                    colorDetailsItem.Name =
                        colorDetailsItem.HashCode =
                        string.Format(
                            "{0}{1}{2}{3}{4}",
                            "#",
                            textBoxCustomColorA.Text.Trim(),
                            textBoxCustomColorR.Text.Trim(),
                            textBoxCustomColorG.Text.Trim(),
                            textBoxCustomColorB.Text.Trim());
                    listBoxColorsListSelected.Items.Add((BoxColorDetails)colorDetailsItem);
                }
            }
            catch { }
        }

        private void buttonDeleteFromList_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if ((BoxColorDetails)listBoxColorsListSelected.SelectedValue != null)
                {
                    listBoxColorsListSelected.Items.Remove((BoxColorDetails)listBoxColorsListSelected.SelectedValue);
                }
            }
            catch { }
        }

        #endregion

        #region comboBoxMsgBoxMethodSilentBox

        private void comboBoxMsgBoxMethodSilentBox_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodSilentBox.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodSilentBox.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodSilentBox.
                var msgBoxMethodSilentBox = EnumExtensions.GetEnumValues(new MsgBoxMethodSilentBox());
                /// Add each one to this collection:
                foreach (var button in msgBoxMethodSilentBox)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = button.ToString();
                    comboBoxMsgBoxMethodSilentBox.Items.Add(comboBoxItemAdded);
                }

                if (comboBoxMsgBoxMethodSilentBox.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodSilentBox.SelectedItem =
                        comboBoxMsgBoxMethodSilentBox.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        private void comboBoxMsgBoxMethodSilentBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodSilentBox.SelectedValue;

                if (comboBoxItemSelected != null)
                {
                    if (comboBoxItemSelected.Content.ToString().ToUpper() == stringCustom.ToUpper())
                    {
                        checkBoxCustomSilentBox.IsChecked = true;
                    }
                }
            }
            catch { }
        }

        private void checkBoxCustomSilentBox_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodSilentBox.Items.Count > 0)
                {
                    comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodSilentBox.Items.GetItemAt(comboBoxMsgBoxMethodSilentBox.Items.Count - 1);
                    comboBoxItemSelected.IsSelected = true;
                }
            }
            catch { }
        }

        private void checkBoxCustomSilentBox_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (comboBoxMsgBoxMethodSilentBox.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodSilentBox.SelectedItem =
                        comboBoxMsgBoxMethodSilentBox.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        #endregion

        #region comboBoxMsgBoxMethodCulture

        private void comboBoxMsgBoxMethodCulture_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                comboBoxMsgBoxMethodCulture.Items.Clear();

                ComboBoxItem comboBoxItemAdded = new ComboBoxItem();

                comboBoxItemAdded.Content = stringNull;
                comboBoxMsgBoxMethodCulture.Items.Add(comboBoxItemAdded);

                /// Get a list of MsgBoxMethodCulture.
                var msgBoxMethodCulture = EnumExtensions.GetEnumValues(new MsgBoxMethodCulture());
                /// Add each one to this collection:
                foreach (var button in msgBoxMethodCulture)
                {
                    comboBoxItemAdded = new ComboBoxItem();
                    comboBoxItemAdded.Content = button.ToString();
                    comboBoxMsgBoxMethodCulture.Items.Add(comboBoxItemAdded);
                }

                if (comboBoxMsgBoxMethodCulture.Items.Count > 0)
                {
                    comboBoxMsgBoxMethodCulture.SelectedItem =
                        comboBoxMsgBoxMethodCulture.Items.GetItemAt(0);
                }
            }
            catch { }
        }

        #endregion

        private void buttonShow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_MsgBoxSelector_Construct.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                string TextMessage = textBoxMessage.Text.Trim();

                comboBoxItemSelected = new ComboBoxItem();

                string Caption = string.Empty;
                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodCaption.SelectedValue;
                if (comboBoxItemSelected.Content.ToString().ToUpper() == stringNull.ToUpper())
                {
                    Caption = string.Empty;
                }
                else if (comboBoxItemSelected.Content.ToString().ToUpper() == stringCustom.ToUpper())
                {
                    Caption = textBoxCustomCaption.Text;
                }
                else
                {
                    Caption = comboBoxItemSelected.Content.ToString();
                }

                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodButtons.SelectedValue;
                MsgBoxMethodButtons buttons =
                    EnumExtensions.GetEnumValue<MsgBoxMethodButtons>(comboBoxItemSelected.Content.ToString());
                string[] customButtons = null;
                if (buttons == MsgBoxMethodButtons.Custom)
                {
                    customButtons = new string[textBoxCustomButtons.LineCount];
                    string customButtonText = string.Empty;
                    int lineCounter = -1;
                    for (int line = 0; line < textBoxCustomButtons.LineCount; line++)
                    {
                        customButtonText = (textBoxCustomButtons.GetLineText(line)).Replace("\r\n", "").Trim();
                        if (customButtonText != null && customButtonText != "")
                        {
                            customButtons[++lineCounter] = customButtonText;
                        }
                    }
                }

                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodIcon.SelectedValue;
                MsgBoxMethodIcon icon =
                   EnumExtensions.GetEnumValue<MsgBoxMethodIcon>(comboBoxItemSelected.Content.ToString());
                BitmapImage customIcon = null;
                if (icon == MsgBoxMethodIcon.Custom)
                {
                    customIcon = new BitmapImage(new Uri(imageCustomIcon.Source.ToString()));
                }

                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodDefaultButton.SelectedValue;
                MsgBoxMethodDefaultButton defaultButton =
                   EnumExtensions.GetEnumValue<MsgBoxMethodDefaultButton>(comboBoxItemSelected.Content.ToString());

                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodOptions.SelectedValue;
                MsgBoxMethodOptions options =
                   EnumExtensions.GetEnumValue<MsgBoxMethodOptions>(comboBoxMsgBoxMethodOptions.SelectedValue.ToString());

                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodButtonsColors.SelectedValue;
                MsgBoxMethodButtonsColors buttonsColors =
                   EnumExtensions.GetEnumValue<MsgBoxMethodButtonsColors>(comboBoxItemSelected.Content.ToString());

                Color[] CustomButtonsColors = null;
                if (buttonsColors == MsgBoxMethodButtonsColors.Custom)
                {
                    if (listBoxColorsListSelected.Items.Count > 0)
                    {
                        CustomButtonsColors = new Color[listBoxColorsListSelected.Items.Count];

                        for (int i = 0; i < listBoxColorsListSelected.Items.Count; i++)
                        {
                            colorDetailsItem = new BoxColorDetails();
                            colorDetailsItem = (BoxColorDetails)listBoxColorsListSelected.Items[i];
                            CustomButtonsColors[i] =
                                (Color)ColorConverter.ConvertFromString(colorDetailsItem.HashCode.ToString());
                        }
                    }
                }

                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodSilentBox.SelectedValue;
                MsgBoxMethodSilentBox silentBox =
                   EnumExtensions.GetEnumValue<MsgBoxMethodSilentBox>(comboBoxItemSelected.Content.ToString());
                string CustomSilentBoxText = string.Empty;
                if (silentBox == MsgBoxMethodSilentBox.Custom)
                {
                    CustomSilentBoxText = textBoxCustomSilentBox.Text;
                }

                comboBoxItemSelected = (ComboBoxItem)comboBoxMsgBoxMethodCulture.SelectedValue;
                string Culture = comboBoxItemSelected.Content.ToString();

                bool ShowHelp = false;
                if (checkBoxShowHelp.IsChecked == true)
                {
                    ShowHelp = true;
                }

                MessageBoxSelector.Show(
                    TextMessage,
                    Caption,
                    buttons, customButtons,
                    icon, customIcon,
                    defaultButton,
                    options,
                    buttonsColors, CustomButtonsColors,
                    silentBox, CustomSilentBoxText,
                    Culture,
                    ShowHelp
                    );
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.UnexpectedError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
