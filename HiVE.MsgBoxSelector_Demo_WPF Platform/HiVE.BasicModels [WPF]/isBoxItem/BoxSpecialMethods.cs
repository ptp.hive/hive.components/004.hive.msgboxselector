﻿using System.ComponentModel;

namespace HiVE.BasicModels.isBoxItem
{
    /// <summary>
    /// BoxSpecialMethods for this [HiVE.MsgBoxSelector - Demo [WPF]] HiVE project.
    /// </summary>
    public static class BoxSpecialMethods
    {
        /// <summary>
        /// Get Special ProductInformation
        /// </summary>
        public enum MethodProductInformation
        {
            [SpecialDescription("HiVE.MsgBoxSelector - Demo [WPF]")]
            [Description("HiVE.MsgBoxSelector - Demo [WPF]")]
            ProductName,
            [SpecialDescription("http://www.HiVE.MsgBoxSelector.Demo.WPF.com/")]
            [Description("www.HiVE.MsgBoxSelector.Demo.WPF.com")]
            ProductWebSite,
            [SpecialDescription("http://HiVE.MsgBoxSelector.Demo.WPF@gmail.com/")]
            [Description("HiVE.MsgBoxSelector.Demo.WPF@gmail.com")]
            ProductEmail,
            [SpecialDescription("http://www.PTP.HiVE.com/HiVE.MsgBoxSelector.Demo.WPF/")]
            [Description("www.PTP.HiVE.com/HiVE.MsgBoxSelector.Demo.WPF")]
            ProductLink,
        }
    }
}
