﻿namespace HiVE.BasicModels.isBoxItem
{
    /// <summary>
    /// DropDownControlsListItemsNode
    /// </summary>
    public class BoxDdcListItemsNode
    {
        public string Group { get; set; }
        public int Value { get; set; }
        public string Display { get; set; }
        public string ToolTip { get; set; }

        public BoxDdcListItemsNode() { }

        public BoxDdcListItemsNode(
            string group,
            int value,
            string display,
            string toolTip)
        {
            this.Group = group;
            this.Value = value;
            this.Display = display;
            this.ToolTip = toolTip;
        }
    }
}
