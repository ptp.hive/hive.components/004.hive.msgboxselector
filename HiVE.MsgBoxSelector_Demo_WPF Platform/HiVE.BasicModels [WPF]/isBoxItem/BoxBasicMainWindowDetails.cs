﻿using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;

namespace HiVE.BasicModels.isBoxItem
{
    public class BoxBasicWindowDetails
    {
        public string MainMenuContent { get; set; }

        public string AssemblyProductVersionMajor { get; set; }
        public string AssemblyCopyright { get; set; }

        public string ProductIsTrialMode { get; set; }

        public MethodCulture BasicCulture { get; set; }

        public System.Windows.FlowDirection BasicFlowDirection { get; set; }

        public BoxBasicWindowDetails() { }

        public BoxBasicWindowDetails(
            string mainMenuContent,

            string assemblyProductVersionMajor,
            string assemblyCopyright,

            string productIsTrialMode,

            System.Windows.FlowDirection basicFlowDirection,
            MethodCulture basicCulture)
        {
            this.MainMenuContent = mainMenuContent;

            this.AssemblyProductVersionMajor = assemblyProductVersionMajor;
            this.AssemblyCopyright = assemblyCopyright;

            this.ProductIsTrialMode = productIsTrialMode;

            this.BasicFlowDirection = basicFlowDirection;
            this.BasicCulture = basicCulture;
        }
    }
}
