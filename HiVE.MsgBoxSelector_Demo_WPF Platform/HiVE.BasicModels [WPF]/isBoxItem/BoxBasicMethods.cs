﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace HiVE.BasicModels.isBoxItem
{
    /// <summary>
    /// Common BoxBasicMethods
    /// </summary>
    public static class BoxBasicMethods
    {
        /// <summary>
        /// Get Common CompanyInformation
        /// </summary>
        public enum MethodCompanyInformation
        {
            [SpecialDescription("http://www.PTP.HiVE.com/")]
            [Description("www.PTP.HiVE.com")]
            CompanyWebSite,
            [SpecialDescription("http://PTP.HiVE@gmail.com/")]
            [Description("PTP.HiVE@gmail.com")]
            CompanyEmail
        }

        /// <summary>
        /// Get Common ProductTypeLicensed
        /// </summary>
        public enum MethodProductTypeLicensed
        {
            [SpecialDescription("A OpenSource Product!")]
            [Description("یک محصول متن‌باز!")]
            OpenSource,
            [SpecialDescription("A FreeWare Product!")]
            [Description("یک محصول رایگان!")]
            FreeWare,
            [SpecialDescription("Personal non-commercial user!")]
            [Description("کاربران شخصی غیر تجاری!")]
            PersonalNonCommercial,
            [SpecialDescription("Commercial product!")]
            [Description("محصول تجاری!")]
            Commercial,
            [SpecialDescription("Custom licensed!")]
            [Description("دارای مجوز سفارشی!")]
            Custom,
            [SpecialDescription("")]
            [Description("")]
            None
        }

        /// <summary>
        /// Get Common ProductTypeTrialMode
        /// </summary>
        public enum MethodProductTypeTrialMode
        {
            [SpecialDescription("Trial")]
            [Description("نسخه‌ی آزمایشی")]
            IsTrialMode,
            [SpecialDescription("")]
            [Description("")]
            IsFullMode,
            [SpecialDescription("Demo")]
            [Description("نسخه‌ی نمایشی")]
            Demo,
            [SpecialDescription("Custom TrialMode!")]
            [Description("سفارشی")]
            Custom,
            [SpecialDescription("")]
            [Description("")]
            None
        }

        /// <summary>
        /// Get Common Cultures
        /// Default = en-US
        /// </summary>
        public enum MethodCulture
        {
            /// <summary>
            /// en-US
            /// </summary>
            Default,
            en_US,
            fa_IR,
            Custom
        }

        /// <summary>
        /// Get Common Dialog Results
        /// </summary>
        [ComVisible(true)]
        public enum MethodDialogResult
        {
            /// Common
            None = -1,
            OK = 0,
            Cancel,
            Abort,
            Retry,
            Ignore,
            Yes,
            No,

            /// BackupClient
            Backup,
            DoNothing,

            /// ConnectionLost
            TryToReconnect,
            TryAgain,
            Connect,
            Exit,

            /// FileManager
            Overwrite,
            OverwriteIfOlder,
            Skip,
            Rename,
            Append,
            Download,
            Upload,

            /// Custom Result
            CustomButton1 = 1001,
            CustomButton2 = 1002,
            CustomButton3 = 1003,
            CustomButton4 = 1004,
            CustomButton5 = 1005,
            CustomButton6 = 1006,
            CustomButton7 = 1007,
            CustomButton8 = 1008,
            CustomButton9 = 1009
        }

        /// <summary>
        /// Get Common Buttons
        /// </summary>
        public enum MethodButtons
        {
            /// Common
            OK,
            OKCancel,
            AbortRetryIgnore,
            YesNoCancel,
            YesNo,
            RetryCancel,

            /// BackupClient
            BackupCancel,
            BackupDownloadDoNothing,

            /// ConnectionLost
            ReconnectExit,
            TryAgainCancel,
            ConnectCancel,

            /// FileManager
            AlreadyExists,
            DownloadCancel,
            UploadCancel,

            /// Custom by user
            Custom
        }

        /// <summary>
        /// Get Common Buttons Text
        /// </summary>
        public enum MethodButtonsText
        {
            /// Common

            [SpecialDescription("OK")]
            [Description("تایید")]
            OK = 0,
            [SpecialDescription("Cancel")]
            [Description("انصراف")]
            Cancel,
            [SpecialDescription("Abort")]
            [Description("بدون نتیجه گذاشتن")]
            Abort,
            [SpecialDescription("Retry")]
            [Description("امتحان مجدد")]
            Retry,
            [SpecialDescription("Ignore")]
            [Description("نادیده گرفتن")]
            Ignore,
            [SpecialDescription("Yes")]
            [Description("بله")]
            Yes,
            [SpecialDescription("No")]
            [Description("خیر")]
            No,

            /// BackupClient

            [SpecialDescription("Backup")]
            [Description("پشتیبان گیری")]
            Backup,
            [SpecialDescription("Do Nothing")]
            [Description("هیچ کاری")]
            DoNothing,

            /// ConnectionLost

            [SpecialDescription("Try To Reconnect")]
            [Description("تلاش برای اتصال مجدد")]
            TryToReconnect,
            [SpecialDescription("Try Again ...")]
            [Description("تلاش مجدد")]
            TryAgain,
            [SpecialDescription("Connect")]
            [Description("اتصال")]
            Connect,
            [SpecialDescription("Exit")]
            [Description("خروج")]
            Exit,

            /// FileManager
            /// AlreadyExists, Download, Upload

            [SpecialDescription("Overwrite")]
            [Description("بازنویسی")]
            Overwrite,
            [SpecialDescription("Overwrite If Older")]
            [Description("بازنویسی در صورت وجود")]
            OverwriteIfOlder,
            [SpecialDescription("Skip")]
            [Description("پریدن")]
            Skip,
            [SpecialDescription("Rename")]
            [Description("تغییر نام")]
            Rename,
            [SpecialDescription("Append")]
            [Description("پیوست کردن")]
            Append,
            [SpecialDescription("Download")]
            [Description("بارگیری")]
            Download,
            [SpecialDescription("Upload")]
            [Description("بارگذاری")]
            Upload,

            /// Custom by user

            [SpecialDescription("Custom")]
            [Description("سفارشی")]
            Custom
        }

        /// <summary>
        /// Get Common Default Buttons
        /// </summary>
        public enum MethodDefaultButton
        {
            Button1,
            Button2,
            Button3,
            Button4,
            Button5,
            Button6,
            Button7,
            Button8,
            Button9,
            None
        }

        /// <summary>
        /// Get Common United Colors
        /// </summary>
        public enum MethodUnitedColors : uint
        {
            #region Method United Colors

            [Description("#00000000")]
            None,                   /// Color.Empty
            [Description("#FFFFFAFA")]
            Default,                /// 0xFFFFFAFA,         /// Snow
            [Description("#FF0044CC")]
            Primary,                /// 0xFF0044CC,         /// RichVein
            [Description("#FF008000")]
            Success,                /// 0xFF008000,         /// Green
            [Description("#FF008B8B")]
            Info,                   /// 0xFF008B8B,         /// DarkCyan
            [Description("#FFFFD700")]
            Warning,                /// 0xFFFFD700,         /// Gold
            [Description("#FFDC143C")]
            Danger,                 /// 0xFFDC143C,         /// Crimson
            [Description("#FF9400D3")]
            Surprise,               /// 0xFF9400D3,         /// DarkViolet
            [Description("#FF202B30")]
            Inverse,                /// 0xFF202B30,         /// Dark
            [Description("#FFAAB2BD")]
            Normal,                 /// 0xFFAAB2BD,         /// MediumGray
            [Description("#FF1E90FF")]
            Link                    /// 0xFF1E90FF,         /// DodgerBlue          JustText

            #endregion
        }

        /// <summary>
        /// Get Common Colors
        /// </summary>
        public enum MethodColors : uint
        {
            #region Method United Colors

            [Description("#00000000")]
            None,                   /// Color.Empty
            [Description("#FFFFFAFA")]
            Default,                /// 0xFFFFFAFA,         /// Snow
            [Description("#FF0044CC")]
            Primary,                /// 0xFF0044CC,         /// RichVein
            [Description("#FF008000")]
            Success,                /// 0xFF008000,         /// Green
            [Description("#FF008B8B")]
            Info,                   /// 0xFF008B8B,         /// DarkCyan
            [Description("#FFFFD700")]
            Warning,                /// 0xFFFFD700,         /// Gold
            [Description("#FFDC143C")]
            Danger,                 /// 0xFFDC143C,         /// Crimson
            [Description("#FF9400D3")]
            Surprise,               /// 0xFF9400D3,         /// DarkViolet
            [Description("#FF202B30")]
            Inverse,                /// 0xFF202B30,         /// Dark
            [Description("#FFAAB2BD")]
            Normal,                 /// 0xFFAAB2BD,         /// MediumGray
            [Description("#FF1E90FF")]
            Link,                   /// 0xFF1E90FF,         /// DodgerBlue          JustText

            #endregion           

            #region Method .NET C# Colors

            [Description("#FFF0F8FF")]
            AliceBlue = 0xFFF0F8FF,
            [Description("#FFFAEBD7")]
            AntiqueWhite = 0xFFFAEBD7,
            [Description("#FF00FFFF")]
            Aqua = 0xFF00FFFF,
            [Description("#FF7FFFD4")]
            Aquamarine = 0xFF7FFFD4,
            [Description("#FFF0FFFF")]
            Azure = 0xFFF0FFFF,
            [Description("#FFF5F5DC")]
            Beige = 0xFFF5F5DC,
            [Description("#FFFFE4C4")]
            Bisque = 0xFFFFE4C4,
            [Description("#FF000000")]
            Black = 0xFF000000,
            [Description("#FFFFEBCD")]
            BlanchedAlmond = 0xFFFFEBCD,
            [Description("#FF0000FF")]
            Blue = 0xFF0000FF,
            [Description("#FF8A2BE2")]
            BlueViolet = 0xFF8A2BE2,
            [Description("#FFA52A2A")]
            Brown = 0xFFA52A2A,
            [Description("#FFDEB887")]
            BurlyWood = 0xFFDEB887,
            [Description("#FF5F9EA0")]
            CadetBlue = 0xFF5F9EA0,
            [Description("#FF7FFF00")]
            Chartreuse = 0xFF7FFF00,
            [Description("#FFD2691E")]
            Chocolate = 0xFFD2691E,
            [Description("#FFFF7F50")]
            Coral = 0xFFFF7F50,
            [Description("#FF6495ED")]
            CornflowerBlue = 0xFF6495ED,
            [Description("#FFFFF8DC")]
            Cornsilk = 0xFFFFF8DC,
            [Description("#FFDC143C")]
            Crimson = 0xFFDC143C,
            [Description("#FF00FFFF")]
            Cyan = 0xFF00FFFF,
            [Description("#FF00008B")]
            DarkBlue = 0xFF00008B,
            [Description("#FF008B8B")]
            DarkCyan = 0xFF008B8B,
            [Description("#FFB8860B")]
            DarkGoldenRod = 0xFFB8860B,
            [Description("#FFA9A9A9")]
            DarkGray = 0xFFA9A9A9,
            [Description("#FF006400")]
            DarkGreen = 0xFF006400,
            [Description("#FFBDB76B")]
            DarkKhaki = 0xFFBDB76B,
            [Description("#FF8B008B")]
            DarkMagenta = 0xFF8B008B,
            [Description("#FF556B2F")]
            DarkOliveGreen = 0xFF556B2F,
            [Description("#FFFF8C00")]
            DarkOrange = 0xFFFF8C00,
            [Description("#FF9932CC")]
            DarkOrchid = 0xFF9932CC,
            [Description("#FF8B0000")]
            DarkRed = 0xFF8B0000,
            [Description("#FFE9967A")]
            DarkSalmon = 0xFFE9967A,
            [Description("#FF8FBC8F")]
            DarkSeaGreen = 0xFF8FBC8F,
            [Description("#FF483D8B")]
            DarkSlateBlue = 0xFF483D8B,
            [Description("#FF2F4F4F")]
            DarkSlateGray = 0xFF2F4F4F,
            [Description("#FF00CED1")]
            DarkTurquoise = 0xFF00CED1,
            [Description("#FF9400D3")]
            DarkViolet = 0xFF9400D3,
            [Description("#FFFF1493")]
            DeepPink = 0xFFFF1493,
            [Description("#FF00BFFF")]
            DeepSkyBlue = 0xFF00BFFF,
            [Description("#FF696969")]
            DimGray = 0xFF696969,
            [Description("#FF1E90FF")]
            DodgerBlue = 0xFF1E90FF,
            [Description("#FFB22222")]
            FireBrick = 0xFFB22222,
            [Description("#FFFFFAF0")]
            FloralWhite = 0xFFFFFAF0,
            [Description("#FF228B22")]
            ForestGreen = 0xFF228B22,
            [Description("#FFFF00FF")]
            Fuchsia = 0xFFFF00FF,
            [Description("#FFDCDCDC")]
            Gainsboro = 0xFFDCDCDC,
            [Description("#FFF8F8FF")]
            GhostWhite = 0xFFF8F8FF,
            [Description("#FFFFD700")]
            Gold = 0xFFFFD700,
            [Description("#FFDAA520")]
            GoldenRod = 0xFFDAA520,
            [Description("#FF808080")]
            Gray = 0xFF808080,
            [Description("#FF008000")]
            Green = 0xFF008000,
            [Description("#FFADFF2F")]
            GreenYellow = 0xFFADFF2F,
            [Description("#FFF0FFF0")]
            HoneyDew = 0xFFF0FFF0,
            [Description("#FFFF69B4")]
            HotPink = 0xFFFF69B4,
            [Description("#FFCD5C5C")]
            IndianRed = 0xFFCD5C5C,
            [Description("#FF4B0082")]
            Indigo = 0xFF4B0082,
            [Description("#FFFFFFF0")]
            Ivory = 0xFFFFFFF0,
            [Description("#FFF0E68C")]
            Khaki = 0xFFF0E68C,
            [Description("#FFE6E6FA")]
            Lavender = 0xFFE6E6FA,
            [Description("#FFFFF0F5")]
            LavenderBlush = 0xFFFFF0F5,
            [Description("#FF7CFC00")]
            LawnGreen = 0xFF7CFC00,
            [Description("#FFFFFACD")]
            LemonChiffon = 0xFFFFFACD,
            [Description("#FFADD8E6")]
            LightBlue = 0xFFADD8E6,
            [Description("#FFF08080")]
            LightCoral = 0xFFF08080,
            [Description("#FFE0FFFF")]
            LightCyan = 0xFFE0FFFF,
            [Description("#FFFAFAD2")]
            LightGoldenRodYellow = 0xFFFAFAD2,
            [Description("#FFD3D3D3")]
            LightGray = 0xFFD3D3D3,
            [Description("#FF90EE90")]
            LightGreen = 0xFF90EE90,
            [Description("#FFFFB6C1")]
            LightPink = 0xFFFFB6C1,
            [Description("#FFFFA07A")]
            LightSalmon = 0xFFFFA07A,
            [Description("#FF20B2AA")]
            LightSeaGreen = 0xFF20B2AA,
            [Description("#FF87CEFA")]
            LightSkyBlue = 0xFF87CEFA,
            [Description("#FF778899")]
            LightSlateGray = 0xFF778899,
            [Description("#FFB0C4DE")]
            LightSteelBlue = 0xFFB0C4DE,
            [Description("#FFFFFFE0")]
            LightYellow = 0xFFFFFFE0,
            [Description("#FF00FF00")]
            Lime = 0xFF00FF00,
            [Description("#FF32CD32")]
            LimeGreen = 0xFF32CD32,
            [Description("#FFFAF0E6")]
            Linen = 0xFFFAF0E6,
            [Description("#FFFF00FF")]
            Magenta = 0xFFFF00FF,
            [Description("#FF800000")]
            Maroon = 0xFF800000,
            [Description("#FF66CDAA")]
            MediumAquaMarine = 0xFF66CDAA,
            [Description("#FF0000CD")]
            MediumBlue = 0xFF0000CD,
            [Description("#FFBA55D3")]
            MediumOrchid = 0xFFBA55D3,
            [Description("#FF9370DB")]
            MediumPurple = 0xFF9370DB,
            [Description("#FF3CB371")]
            MediumSeaGreen = 0xFF3CB371,
            [Description("#FF7B68EE")]
            MediumSlateBlue = 0xFF7B68EE,
            [Description("#FF00FA9A")]
            MediumSpringGreen = 0xFF00FA9A,
            [Description("#FF48D1CC")]
            MediumTurquoise = 0xFF48D1CC,
            [Description("#FFC71585")]
            MediumVioletRed = 0xFFC71585,
            [Description("#FF191970")]
            MidnightBlue = 0xFF191970,
            [Description("#FFF5FFFA")]
            MintCream = 0xFFF5FFFA,
            [Description("#FFFFE4E1")]
            MistyRose = 0xFFFFE4E1,
            [Description("#FFFFE4B5")]
            Moccasin = 0xFFFFE4B5,
            [Description("#FFFFDEAD")]
            NavajoWhite = 0xFFFFDEAD,
            [Description("#FF000080")]
            Navy = 0xFF000080,
            [Description("#FFFDF5E6")]
            OldLace = 0xFFFDF5E6,
            [Description("#FF808000")]
            Olive = 0xFF808000,
            [Description("#FF6B8E23")]
            OliveDrab = 0xFF6B8E23,
            [Description("#FFFFA500")]
            Orange = 0xFFFFA500,
            [Description("#FFFF4500")]
            OrangeRed = 0xFFFF4500,
            [Description("#FFDA70D6")]
            Orchid = 0xFFDA70D6,
            [Description("#FFEEE8AA")]
            PaleGoldenRod = 0xFFEEE8AA,
            [Description("#FF98FB98")]
            PaleGreen = 0xFF98FB98,
            [Description("#FFAFEEEE")]
            PaleTurquoise = 0xFFAFEEEE,
            [Description("#FFDB7093")]
            PaleVioletRed = 0xFFDB7093,
            [Description("#FFFFEFD5")]
            PapayaWhip = 0xFFFFEFD5,
            [Description("#FFFFDAB9")]
            PeachPuff = 0xFFFFDAB9,
            [Description("#FFCD853F")]
            Peru = 0xFFCD853F,
            [Description("#FFFFC0CB")]
            Pink = 0xFFFFC0CB,
            [Description("#FFDDA0DD")]
            Plum = 0xFFDDA0DD,
            [Description("#FFB0E0E6")]
            PowderBlue = 0xFFB0E0E6,
            [Description("#FF800080")]
            Purple = 0xFF800080,
            [Description("#FF663399")]
            RebeccaPurple = 0xFF663399,
            [Description("#FFFF0000")]
            Red = 0xFFFF0000,
            [Description("#FFBC8F8F")]
            RosyBrown = 0xFFBC8F8F,
            [Description("#FF4169E1")]
            RoyalBlue = 0xFF4169E1,
            [Description("#FF8B4513")]
            SaddleBrown = 0xFF8B4513,
            [Description("#FFFA8072")]
            Salmon = 0xFFFA8072,
            [Description("#FFF4A460")]
            SandyBrown = 0xFFF4A460,
            [Description("#FF2E8B57")]
            SeaGreen = 0xFF2E8B57,
            [Description("#FFFFF5EE")]
            SeaShell = 0xFFFFF5EE,
            [Description("#FFA0522D")]
            Sienna = 0xFFA0522D,
            [Description("#FFC0C0C0")]
            Silver = 0xFFC0C0C0,
            [Description("#FF87CEEB")]
            SkyBlue = 0xFF87CEEB,
            [Description("#FF6A5ACD")]
            SlateBlue = 0xFF6A5ACD,
            [Description("#FF708090")]
            SlateGray = 0xFF708090,
            [Description("#FFFFFAFA")]
            Snow = 0xFFFFFAFA,
            [Description("#FF00FF7F")]
            SpringGreen = 0xFF00FF7F,
            [Description("#FF4682B4")]
            SteelBlue = 0xFF4682B4,
            [Description("#FFD2B48C")]
            Tan = 0xFFD2B48C,
            [Description("#FF008080")]
            Teal = 0xFF008080,
            [Description("#FFD8BFD8")]
            Thistle = 0xFFD8BFD8,
            [Description("#FFFF6347")]
            Tomato = 0xFFFF6347,
            [Description("#FF40E0D0")]
            Turquoise = 0xFF40E0D0,
            [Description("#FFEE82EE")]
            Violet = 0xFFEE82EE,
            [Description("#FFF5DEB3")]
            Wheat = 0xFFF5DEB3,
            [Description("#FFFFFFFF")]
            White = 0xFFFFFFFF,
            [Description("#FFF5F5F5")]
            WhiteSmoke = 0xFFF5F5F5,
            [Description("#FFFFFF00")]
            Yellow = 0xFFFFFF00,
            [Description("#FF9ACD32")]
            YellowGreen = 0xFF9ACD32,

            #endregion

            #region Method Custom Colors

            [Description("#FFE9573F")]
            BitterSweet = 0xFFE9573F,
            [Description("#FF4A89DC")]
            BlueJeans = 0xFF4A89DC,
            [Description("#FF0247FE")]
            BlueRYB = 0xFF0247FE,
            [Description("#FF202B30")]
            Dark = 0xFF202B30,
            [Description("#FF1F262A")]
            DarkGunmetal = 0xFF1F262A,
            [Description("#FFDA4453")]
            Grapefruit = 0xFFDA4453,
            [Description("#FF8CC152")]
            Grass = 0xFF8CC152,
            [Description("#FF0088CC")]
            HelloSky = 0xFF0088CC,
            [Description("#FFBD362F")]
            HotMayDay = 0xFFBD362F,
            [Description("#FFAAB2BD")]
            MediumGray = 0xFFAAB2BD,
            [Description("#FF37BC9B")]
            Mint = 0xFF37BC9B,
            [Description("#FFD770AD")]
            PinkRose = 0xFFD770AD,
            [Description("#FF26133C")]
            RaisinBlack = 0xFF26133C,
            [Description("#FF0044CC")]
            RichVein = 0xFF0044CC,
            [Description("#FFF5F5F5")]
            Smoke = 0xFFF5F5F5,
            [Description("#FF989898")]
            SpanishGray = 0xFF989898,
            [Description("#FFF6BB42")]
            Sunflower = 0xFFF6BB42,

            #endregion
        }

        /// <summary>
        /// Get Common Icons
        /// </summary>
        public enum MethodIcon
        {
            None,
            Asterisk,
            Error,
            Exclamation,
            Hand,
            Information,
            Question,
            Stop,
            Warning,
            Shield,
            WinLogo,
            /// <summary>
            /// Icon in the window header. By default it is the icon of the application.
            /// For this Error: "Argument 'picture' must be a picture that can be used as a Icon."
            /// Make sure that use of a Save As 24-bit Bitmap(*.bmp;*dib) format.
            /// Other formats cannot be used as icons, so that it all there is to it.
            /// </summary>
            ApplicationIcon,
            Custom
        }

        /// <summary>
        /// Get Common Captions
        /// </summary>
        public enum MethodCaption : uint
        {
            [Description("نام محصول")]
            ProductName,

            [Description("اطلاعات")]
            Information,
            [Description("سوال")]
            Question,
            [Description("هشدار")]
            Warning,
            [Description("خطا")]
            Error,
            [Description("ایست")]
            Stop,
            [Description("محافظ")]
            Shield,
            [Description("راهنمایی")]
            Help,
            [Description("راهنما")]
            Guide,

            [Description("مالک")]
            Admin,
            [Description("مدیر")]
            Administrator,
            [Description("مدیریت")]
            Management,

            [Description("مجوز")]
            Licenced,
            [Description("تصدیق")]
            Authentication,
            [Description("سطح دسترسی")]
            Authorization,
            [Description("صحت")]
            Validation,
            [Description("ورود")]
            Login,
            [Description("خروج")]
            Logout,

            [Description("پشتیبان")]
            Backup,
            [Description("بارگيرى")]
            Download,
            [Description("بارگزاری")]
            Upload,
            [Description("پیوست")]
            Attach,
            [Description("گزارش")]
            Report,
            [Description("اتصال")]
            Connect,
            [Description("پایگاه‌داده")]
            DataBase,

            [Description("نشان")]
            Asterisk,
            [Description("تعجب")]
            Exclamation,
            [Description("دست")]
            Hand
        }

        /// <summary>
        /// Get Common Options
        /// </summary>
        [Flags]
        public enum MethodOptions
        {
            DefaultDesktopOnly = 0x20000,
            RightAlign = 0x80000,
            RtlReading = 0x100000,
            ServiceNotification = 0x200000
        }

        /// <summary>
        /// Get Common SilentBoxs
        /// </summary>
        public enum MethodSilentBox
        {
            [SpecialDescription("")]
            [Description("")]
            None,
            [SpecialDescription("Apply to all")]
            [Description("اعمال به همه")]
            ApplyToAll,
            [SpecialDescription("Skip all")]
            [Description("پرش همه")]
            SkipAll,
            /// <summary>
            /// Text for repeat-my-answer-in-the future checkbox
            /// Repeat my answer for such cases during this session
            /// </summary>
            [SpecialDescription("Repeat")]
            [Description("بازگو")]
            Repeat,
            Custom
        }

        /// <summary>
        /// Get Common GroupNames
        /// </summary>
        public enum MethodGroupName
        {
            Group1,
            Group2,
            Group3,
            Group4,
            Group5,
            Group6,
            Group7,
            Group8,
            Group9,
            None
        }

        /// <summary>
        /// Get Common RadioButtons
        /// </summary>
        public enum MethodRadioButtons
        {
            RadioButton01,
            RadioButton02,
            RadioButton03,
            RadioButton04,
            RadioButton05,
            RadioButton06,
            RadioButton07,
            RadioButton08,
            RadioButton09,
            RadioButton10,
            RadioButton11,
            RadioButton12,
            RadioButton13,
            RadioButton14,
            RadioButton15,
            RadioButton16,
            RadioButton17,
            RadioButton18,
            RadioButton19,
            None
        }
    }
}
