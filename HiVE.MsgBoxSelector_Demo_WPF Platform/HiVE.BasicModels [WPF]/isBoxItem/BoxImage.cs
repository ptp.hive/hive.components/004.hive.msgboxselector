﻿namespace HiVE.BasicModels.isBoxItem
{
    public class BoxImage
    {
        public int ImageId { get; set; }
        public string Title { get; set; }
        public byte[] Avatar { get; set; }
        public bool IsOmissible { get; set; }

        public BoxImage() { }

        public BoxImage(
            int imageId,
            string title,
            byte[] avatar,
            bool isOmissible)
        {
            this.ImageId = imageId;
            this.Title = title;
            this.Avatar = avatar;
            this.IsOmissible = isOmissible;
        }
    }
}
