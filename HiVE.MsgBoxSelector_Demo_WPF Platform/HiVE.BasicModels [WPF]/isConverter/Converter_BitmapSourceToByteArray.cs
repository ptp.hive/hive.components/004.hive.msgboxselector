﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_BitmapSourceToByteArray
    {
        public static byte[] ConvertBitmapSourceToByteArray(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;

            try
            {
                var bitmapSource = imageSource as BitmapSource;

                if (bitmapSource != null)
                {
                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                    using (var stream = new MemoryStream())
                    {
                        encoder.Save(stream);
                        bytes = stream.ToArray();
                    }
                }
            }
            catch { }

            return bytes;
        }

        public static byte[] ConvertBitmapSourceToByteArray(BitmapSource image)
        {
            byte[] data = null;

            try
            {
                BitmapEncoder encoder = new PngBitmapEncoder(); /// or one of the other encoders
                encoder.Frames.Add(BitmapFrame.Create(image));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                }
            }
            catch { }

            return data;
        }

        public static byte[] ConvertBitmapSourceToByteArray(ImageSource imageSource)
        {
            byte[] data = null;

            try
            {
                var image = imageSource as BitmapSource;
                BitmapEncoder encoder = new PngBitmapEncoder(); /// or one of the other encoders
                encoder.Frames.Add(BitmapFrame.Create(image));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                }
            }
            catch { }

            return data;
        }

        public static byte[] ConvertBitmapSourceToByteArray(Uri uri)
        {
            byte[] data = null;

            try
            {
                var image = new BitmapImage(uri);
                BitmapEncoder encoder = new PngBitmapEncoder(); /// or one of the other encoders
                encoder.Frames.Add(BitmapFrame.Create(image));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                }
            }
            catch { }

            return data;
        }

        public static byte[] ConvertBitmapSourceToByteArray(string filepath)
        {
            byte[] data = null;

            try
            {
                var image = new BitmapImage(new Uri(filepath));
                BitmapEncoder encoder = new PngBitmapEncoder(); /// or one of the other encoders
                encoder.Frames.Add(BitmapFrame.Create(image));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                }
            }
            catch { }

            return data;
        }

        public static BitmapImage ConvertByteArrayToBitmapImage(Byte[] bytes)
        {
            var image = new BitmapImage();

            try
            {
                var stream = new MemoryStream(bytes);
                stream.Seek(0, SeekOrigin.Begin);
                image.BeginInit();
                image.StreamSource = stream;
                image.EndInit();
            }
            catch { }

            return image;
        }
    }
}
