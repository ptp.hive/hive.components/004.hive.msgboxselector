﻿using System;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_ActualHeightToHeightMinusToolStripLogo : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            double returnValue = 0;

            try
            {
                if (value != null)
                { returnValue = ((double)value - 220); }

                if (returnValue < 0)
                { returnValue = 0; }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from HeightMinusToolStripLogo back to ActualHeight");
        }
    }
}
