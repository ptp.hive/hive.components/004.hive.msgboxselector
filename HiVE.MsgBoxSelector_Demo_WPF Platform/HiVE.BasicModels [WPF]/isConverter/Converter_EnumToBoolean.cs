﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_EnumToBoolean : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                var ParameterString = parameter as string;
                if (ParameterString == null)
                { return DependencyProperty.UnsetValue; }

                if (Enum.IsDefined(value.GetType(), value) == false)
                { return DependencyProperty.UnsetValue; }

                object paramvalue = Enum.Parse(value.GetType(), ParameterString);
                return paramvalue.Equals(value);
            }
            catch { return null; }
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                var ParameterString = parameter as string;
                var valueAsBool = (bool)value;

                if (ParameterString == null || !valueAsBool)
                {
                    try
                    {
                        return Enum.Parse(targetType, "0");
                    }
                    catch (Exception)
                    {
                        return DependencyProperty.UnsetValue;
                    }
                }
                return Enum.Parse(targetType, ParameterString);
            }
            catch { return null; }
        }
    }
}
