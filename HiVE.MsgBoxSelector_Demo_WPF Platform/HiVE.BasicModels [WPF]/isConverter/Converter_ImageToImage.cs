﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_ImageToImage
    {
        public static BitmapFrame CreateResizedImage(ImageSource source, int width, int height, int margin)
        {
            try
            {
                var rect = new Rect(margin, margin, width - margin * 2, height - margin * 2);

                var group = new DrawingGroup();
                RenderOptions.SetBitmapScalingMode(group, BitmapScalingMode.HighQuality);
                group.Children.Add(new ImageDrawing(source, rect));

                var drawingVisual = new DrawingVisual();
                using (var drawingContext = drawingVisual.RenderOpen())
                    drawingContext.DrawDrawing(group);

                var resizedImage = new RenderTargetBitmap(
                    width, height,         // Resized dimensions
                    96, 96,                // Default DPI values
                    PixelFormats.Default); // Default pixel format
                resizedImage.Render(drawingVisual);

                return BitmapFrame.Create(resizedImage);
            }
            catch { return null; }
        }

        public static ImageSource GetGlowingImage(string fileNamePath)
        {
            BitmapImage glowIcon = new BitmapImage();

            try
            {
                glowIcon.BeginInit();
                glowIcon.UriSource = new Uri("pack://application:,,," + fileNamePath);
                glowIcon.EndInit();
            }
            catch { }

            return glowIcon;
        }

        public static Uri GetUriSource(string fileNamePath)
        {
            Uri UriSource = null;

            try
            {
                UriSource = new Uri("pack://application:,,," + fileNamePath, UriKind.Relative);
            }
            catch { }

            return UriSource;
        }

        public static BitmapImage ConvertByteArrayToBitmapImage(Byte[] bytes)
        {
            var image = new BitmapImage();

            try
            {
                var stream = new MemoryStream(bytes);
                stream.Seek(0, SeekOrigin.Begin);
                image.BeginInit();
                image.StreamSource = stream;
                image.EndInit();
            }
            catch { }

            return image;
        }

        public static byte[] ReadImageFile(string imageLocation)
        {
            byte[] imageData = null;

            try
            {
                FileInfo fileInfo = new FileInfo(imageLocation);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(imageLocation, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);
            }
            catch { }

            return imageData;
        }
    }
}
