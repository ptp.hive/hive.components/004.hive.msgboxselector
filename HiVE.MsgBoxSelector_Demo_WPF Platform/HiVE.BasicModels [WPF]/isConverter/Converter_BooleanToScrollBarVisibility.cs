﻿using System;
using System.Windows.Controls;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_BooleanToScrollBarVisibility : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            ScrollBarVisibility returnValue = ScrollBarVisibility.Auto;

            try
            {
                if (value != null)
                {
                    if ((bool)value == true)
                    { returnValue = ScrollBarVisibility.Disabled; }
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Visibility back to IsChecked");
        }
    }
}
