﻿using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;
using static HiVE.MsgBoxSelector.MsgBoxSpecialMethods;

namespace HiVE.BasicModels.isClass
{
    public static class BasicMethods
    {
        /// <summary>
        /// Get or set Basic Culture
        /// Default = MethodCulture.fa_IR
        /// </summary>
        private static MethodCulture _basicCulture = MethodCulture.fa_IR;

        /// <summary>
        /// Get or set Basic Culture
        /// Default = MethodCulture.fa_IR
        /// </summary>
        public static MethodCulture BasicCulture
        {
            get { return (_basicCulture); }
            set { _basicCulture = value; }
        }

        /// <summary>
        /// Get or set Basic FlowDirection
        /// Default = System.Windows.FlowDirection.LeftToRight
        /// </summary>
        private static System.Windows.FlowDirection _basicFlowDirection = System.Windows.FlowDirection.LeftToRight;

        /// <summary>
        /// Get or set Basic FlowDirection
        /// Default = System.Windows.FlowDirection.RightToLeft
        /// </summary>
        public static System.Windows.FlowDirection BasicFlowDirection
        {
            get { return (_basicFlowDirection); }
            set { _basicFlowDirection = value; }
        }

        /// <summary>
        /// Get or set Basic MsgBoxCulture
        /// Default = MsgBoxCulture.fa_IR
        /// </summary>
        private static string _basicMsgBoxCulture = MsgBoxMethodCulture.fa_IR.ToString();

        /// <summary>
        /// Get or set Basic MsgBoxCulture
        /// Default = MsgBoxCulture.fa_IR
        /// </summary>
        public static string BasicMsgBoxCulture
        {
            get { return (_basicMsgBoxCulture); }
            set { _basicMsgBoxCulture = value; }
        }

        /// <summary>
        /// Get or set Basic Show Friendly Message
        /// Default = true
        /// </summary>
        private static bool _isShowFriendlyMessage = true;

        /// <summary>
        /// Get or set Basic Show Friendly Message
        /// Default = true
        /// </summary>
        public static bool IsShowFriendlyMessage
        {
            get { return _isShowFriendlyMessage; }
            set { value = _isShowFriendlyMessage; }
        }

        /// <summary>
        /// Get or set Basic MainMenuContent
        /// Default = string.Empty;
        /// </summary>
        private static string _basicMainMenuContent = string.Empty;

        /// <summary>
        /// Get or set Basic MainMenuContent
        /// Default = string.Empty;
        /// </summary>
        public static string BasicMainMenuContent
        {
            get
            {
                try
                {
                    if (BasicCulture == MethodCulture.fa_IR)
                    {
                        _basicMainMenuContent = "منوی اصلی";
                    }
                    /// <summary>
                    /// if(culture.ToUpper() == BasicCulture.Default.ToString().ToUpper())
                    /// OR if(culture.ToUpper() == BasicCulture.en_US.ToString().ToUpper())
                    /// OR if(culture.ToUpper() == BasicCulture.Custom.ToString().ToUpper())
                    /// OR another case
                    /// </summary>
                    else
                    {
                        _basicMainMenuContent = "Main Menu";
                    }
                }
                catch { }

                return _basicMainMenuContent;
            }
            set
            {
                _basicMainMenuContent = value;
            }
        }

        /// <summary>
        /// Set Basic MainMenuContent
        /// Default = string.Empty;
        /// </summary>
        public static string SetBasicMainMenuContent(MethodCulture culture)
        {
            try
            {
                if (culture == MethodCulture.fa_IR)
                {
                    _basicMainMenuContent = "منوی اصلی";
                }
                /// <summary>
                /// if(culture.ToUpper() == BasicCulture.Default.ToString().ToUpper())
                /// OR if(culture.ToUpper() == BasicCulture.en_US.ToString().ToUpper())
                /// OR if(culture.ToUpper() == BasicCulture.Custom.ToString().ToUpper())
                /// OR another case
                /// </summary>
                else
                {
                    _basicMainMenuContent = "Main Menu";
                }
            }
            catch { }

            return _basicMainMenuContent;
        }
    }
}
