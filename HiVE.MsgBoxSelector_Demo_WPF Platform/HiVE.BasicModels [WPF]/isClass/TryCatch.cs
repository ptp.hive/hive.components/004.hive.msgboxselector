﻿using HiVE.BasicModels.isBoxItem;
using HiVE.MsgBoxSelector;
using System.ComponentModel;
using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;

namespace HiVE.BasicModels.isClass
{
    public static class TryCatch
    {
        #region TryCatch Methods

        /// <summary>
        /// CatchExceptionMessageError
        /// </summary>
        /// <param name="boxTryCatch"></param>
        /// <returns></returns>
        private static BoxTryCatch CatchExceptionMessageError(BoxTryCatch boxTryCatch)
        {
            try
            {
                if (boxTryCatch != null)
                {
                    if (boxTryCatch.IsHaveError)
                    {
                        string finalMessageError = string.Empty;

                        if (boxTryCatch.MessageError.Trim() != "")
                        {
                            boxTryCatch.FinalMessageError +=
                                string.Format(
                                    "{0}{1}_{2}_{3}_{4}_{5} : {6}",
                                    TcMethodCommonMessage.BeginningErrorMessage.ToDescriptionString(),

                                    EnumExtensions.GetEnumMemberIndex(boxTryCatch.TryCatchMethodAssemblyProduct),
                                    EnumExtensions.GetEnumMemberIndex(boxTryCatch.TryCatchMethodTypeTemplate),
                                    boxTryCatch.TryCatchMethodTypeTemplate_Template,
                                    boxTryCatch.TryCatchMethodClassFunctions_Function,
                                    EnumExtensions.GetEnumMemberIndex(boxTryCatch.TryCatchMethodFollowingFunction),

                                    boxTryCatch.MessageError.Trim()
                                    );
                        }

                        if (boxTryCatch.FriendlyMessageError.Trim() != "")
                        {
                            boxTryCatch.FinalFriendlyMessageError +=
                                string.Format(
                                    "{0}{1}_{2}_{3}_{4}_{5} : {6}",
                                    TcMethodCommonMessage.BeginningErrorMessage.ToDescriptionString(),

                                    EnumExtensions.GetEnumMemberIndex(boxTryCatch.TryCatchMethodAssemblyProduct),
                                    EnumExtensions.GetEnumMemberIndex(boxTryCatch.TryCatchMethodTypeTemplate),
                                    boxTryCatch.TryCatchMethodTypeTemplate_Template,
                                    boxTryCatch.TryCatchMethodClassFunctions_Function,
                                    EnumExtensions.GetEnumMemberIndex(boxTryCatch.TryCatchMethodFollowingFunction),

                                    boxTryCatch.FriendlyMessageError.Trim()
                                    );
                        }

                        if (boxTryCatch.IsShowFriendlyMessage)
                        {
                            finalMessageError = boxTryCatch.FinalFriendlyMessageError.Trim();
                        }
                        else
                        {
                            finalMessageError = boxTryCatch.FinalMessageError.Trim();
                        }

                        if (boxTryCatch.IsShowFinalMessageError && finalMessageError.Trim() != "")
                        {
                            boxTryCatch.MessageError = string.Empty;
                            boxTryCatch.FriendlyMessageError = string.Empty;

                            boxTryCatch.FinalMessageError = string.Empty;
                            boxTryCatch.FinalFriendlyMessageError = string.Empty;

                            boxTryCatch.IsHaveError = false;

                            MessageBoxSelector.Show(
                                finalMessageError,
                                MethodCaption.Error.ToDescriptionString(),
                                MsgBoxSpecialMethods.MsgBoxMethodButtons.OK,
                                MsgBoxSpecialMethods.MsgBoxMethodIcon.Error,
                                MsgBoxSpecialMethods.MsgBoxMethodDefaultButton.Button1,
                                boxTryCatch.MsgBoxCulture);
                        }
                    }
                }
            }
            catch { }

            return boxTryCatch;
        }

        /// <summary>
        /// Get CatchExceptionMessageError
        /// </summary>
        /// <param name="boxTryCatch"></param>
        /// <returns></returns>
        public static BoxTryCatch GetCEM_Error(BoxTryCatch boxTryCatch)
        {
            return CatchExceptionMessageError(boxTryCatch);
        }

        #endregion

        #region TryCatch Box Methods

        public enum TcMethodCommonMessage
        {
            [Description("\n- خطای x_")]
            BeginningErrorMessage,
            [Description("\n\n- متاسفیم.")]
            ApologyMessage
        }

        public enum TcMethodGeneralErrorMessage
        {
            [SpecialDescription("Logic Error!")]
            [Description("خطای منطقی.")]
            LogicError,
            [SpecialDescription("An unexpected error!")]
            [Description("یک خطای غیر منتظره!")]
            UnexpectedError,

            [SpecialDescription("Please close all message boxes opened by this application, before using this window!")]
            [Description("لطفا تمام جعبه پیام باز شده توسط این نرم افزار را ببندید، قبل از استفاده از این پنجره!")]
            ShowBlockingDialog,
            [SpecialDescription("Could not extract default icon for MessageBox Window!")]
            [Description("نماد پیش فرض برای پیام جعبه پنجره استخراج نشده!")]
            CouldNotExtractIcon,

            [Description("خطا در بارگیری برنامه.")]
            LoadingWindowError,
            [Description("خطا در بارگیری برنامه.")]
            LoadingUserControlError,

            [SpecialDescription("Error loading input file path!")]
            [Description("خطا در بارگیری مسیر فایل ورودی.")]
            LoadingInputFilePathError,

            [Description("خطا در ایجاد پایگاه‌داده.")]
            ErrorModelCreatingDatabase,
            [Description("خطا در مقداردهی اولیه به اطلاعات پایگاه‌داده.")]
            ErrorSeedInitialValue,
            [Description("خطا در برقراری ارتباط با پایگاه‌داده.")]
            ErrorConnectionDatabase,
            [Description("خطا در بارگیری اطلاعات صندوق.")]
            ErrorLoadInformationsDatabase,

            [Description("{0} با این {1} پیدا نشد!")]
            ErrorNotFoundWithThis_2
        }

        public enum TcMethodTypeTemplate
        {
            IsOthers = 0,
            IsWindow,
            IsForm,
            IsPage,
            IsUserControl,
            IsClass
        }

        public enum TcMethodFollowingFunction
        {
            FollowingFunction1 = 1,
            FollowingFunction2,
            FollowingFunction3,
            FollowingFunction4,
            FollowingFunction5,
            FollowingFunction6,
            FollowingFunction7,
            FollowingFunction8,
            FollowingFunction9,
            FollowingFunction_Others,
        }

        public enum TcMethodAssemblyProduct
        {
            /// Common
            HiVE_BasicModels = 0,
            HiVE_Prerequisites,
            HiVE_DatabaseContext,
            HiVE_Startup,
            HiVE_Login,
            HiVE_MainProject,
            /// HiVE_MainProject
            HiVE_MsgBoxSelector_Demo_WPF,

            /// Others

        }

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ All Assembly Products

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_BasicModels

        public enum TcMethodTypeTemplate_HiVE_BasicModels_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_BasicModels_IsClass
        {
            #region isClass

            BasicAssemblyAttributeAccessors

            #endregion

            #region isConverter

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_Prerequisites

        public enum TcMethodTypeTemplate_HiVE_Prerequisites_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_Prerequisites_IsClass
        {
            #region isClass

            FontOperation,
            RunPrerequisites

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_DatabaseContext

        public enum TcMethodTypeTemplate_HiVE_DatabaseContext_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_DatabaseContext_IsClass
        {
            #region isClass

            #region isDb_Context

            DatabaseContext,
            DatabaseContextInitializer,
            isMigrations_Configuration,

            #endregion

            #region isDb_oDataSet

            is_oBasic_Syntax,

            #endregion

            #region isDb_Tables

            #endregion

            #region isDb_Triggers

            #endregion

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_Startup

        public enum TcMethodTypeTemplate_HiVE_Startup_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsUserControl
        {
            #region isUserControl

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_Startup_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            SpecialModels

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_Login

        public enum TcMethodTypeTemplate_HiVE_Login_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsUserControl
        {
            #region isUserControl

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_Login_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            SpecialModels

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_MainProject

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsUserControl
        {
            #region isUserControl

            About

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_MainProject_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            SpecialModels

            #endregion
        }

        #endregion

        #region TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ HiVE_MsgBoxSelector_Demo_WPF

        public enum TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsOthers
        {

        }

        public enum TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsWindow
        {
            MainWindow = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsForm
        {
            MainForm = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsPage
        {
            MainPage = 0
        }

        public enum TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl
        {
            #region isUserControl

            About,
            ColorListBox,
            MsgBoxSelector_Construct,
            MsgBoxSelector_Default

            #endregion
        }

        public enum TcMethodTypeTemplate_HiVE_MsgBoxSelector_Demo_WPF_IsClass
        {
            #region Models

            AssemblyAttributeAccessors,
            SpecialModels

            #endregion
        }

        #endregion

        #endregion /TcMethodTypeTemplate_[AssemblyProduct]_[TypeTemplate] _ All Assembly Products

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ All Assembly Products

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_BasicModels

        #region TcMethodClassFunctions_HiVE_BasicModels_IsClass

        #region TcMethodClassFunctions_HiVE_BasicModels_IsClass_isClass

        public enum TcMethodClassFunctions_HiVE_BasicModels_IsClass_BasicAssemblyAttributeAccessors
        {

        }

        #endregion

        #region TcMethodClassFunctions_HiVE_BasicModels_IsClass_isConverter

        public enum TcMethodClassFunctions_HiVE_BasicModels_IsClass_Converter_BitmapSourceToByteArray
        {

        }

        #endregion

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_Prerequisites

        #region TcMethodClassFunctions_HiVE_Prerequisites_IsClass

        public enum TcMethodClassFunctions_HiVE_Prerequisites_IsClass_FontOperation
        {

        }

        public enum TcMethodClassFunctions_HiVE_Prerequisites_IsClass_RunPrerequisites
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_DatabaseContext

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_Context

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_DatabaseContext
        {
            DatabaseContext,
            OnModelCreating,
            DbModelBuilder_Configurations_Tables
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_DatabaseContextInitializer
        {
            Seed,
            SeedInitialValue_Tables
        }

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isMigrations_Configuration
        {
            Seed
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_oDataSet

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_is_oBasic_Syntax
        {
            InitialValue,
            AddDefaultBasic_Syntaxs
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_Tables

        #endregion

        #region TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_isClass_isDb_Triggers

        public enum TcMethodClassFunctions_HiVE_DatabaseContext_IsClass_Triggers_Configuration
        {

        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_Startup

        #region TcMethodClassFunctions_HiVE_Startup_IsWindow

        public enum TcMethodClassFunctions_HiVE_Startup_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_Startup_IsClass

        public enum TcMethodClassFunctions_Startup_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_Startup_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_Login

        #region TcMethodClassFunctions_HiVE_Login_IsWindow

        public enum TcMethodClassFunctions_HiVE_Login_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_Login_IsClass

        public enum TcMethodClassFunctions_Login_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_Login_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_MainProject

        #region TcMethodClassFunctions_HiVE_MainProject_IsWindow

        public enum TcMethodClassFunctions_HiVE_MainProject_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_MainProject_IsUserControl

        public enum TcMethodClassFunctions_HiVE_MainProject_IsUserControl_About
        {
            ThisUserControl_Loaded = 0
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_MainProject_IsClass

        public enum TcMethodClassFunctions_HiVE_MainProject_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_HiVE_MainProject_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #region TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ HiVE_MsgBoxSelector_Demo_WPF

        #region TcMethodClassFunctions_HiVE_MainProject_IsWindow

        public enum TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsWindow_MainWindow
        {
            ThisWindow_Loaded,
            DisplayBasicWindowDetails
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl

        public enum TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_About
        {
            ThisUserControl_Loaded = 0
        }

        public enum TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_ColorListBox
        {
            ThisUserControl_Loaded = 0
        }

        public enum TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_MsgBoxSelector_Construct
        {
            ThisUserControl_Loaded = 0
        }

        public enum TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsUserControl_MsgBoxSelector_Default
        {
            ThisUserControl_Loaded = 0
        }

        #endregion

        #region TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsClass

        public enum TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsClass_AssemblyAttributeAccessors
        {

        }

        public enum TcMethodClassFunctions_HiVE_MsgBoxSelector_Demo_WPF_IsClass_SpecialModels
        {

        }

        #endregion

        #endregion

        #endregion /TcMethodClassFunctions_[AssemblyProduct]_[TypeTemplate]_[ClassFunctions] _ All Assembly Products

        #endregion /TryCatch Box Methods
    }
}
