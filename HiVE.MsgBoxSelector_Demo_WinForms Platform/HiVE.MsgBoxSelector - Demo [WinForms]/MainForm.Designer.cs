﻿namespace HiVE.MsgBoxSelector_Demo_WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanelMain = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonSample01 = new System.Windows.Forms.Button();
            this.buttonSample02 = new System.Windows.Forms.Button();
            this.buttonSample03 = new System.Windows.Forms.Button();
            this.buttonSample04 = new System.Windows.Forms.Button();
            this.buttonSample05 = new System.Windows.Forms.Button();
            this.buttonSample06 = new System.Windows.Forms.Button();
            this.buttonSample07 = new System.Windows.Forms.Button();
            this.tableLayoutPanelMain.SuspendLayout();
            this.flowLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 3;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanelMain, 1, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 3;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(704, 441);
            this.tableLayoutPanelMain.TabIndex = 5;
            // 
            // flowLayoutPanelMain
            // 
            this.flowLayoutPanelMain.AutoSize = true;
            this.flowLayoutPanelMain.Controls.Add(this.buttonSample01);
            this.flowLayoutPanelMain.Controls.Add(this.buttonSample02);
            this.flowLayoutPanelMain.Controls.Add(this.buttonSample03);
            this.flowLayoutPanelMain.Controls.Add(this.buttonSample04);
            this.flowLayoutPanelMain.Controls.Add(this.buttonSample05);
            this.flowLayoutPanelMain.Controls.Add(this.buttonSample06);
            this.flowLayoutPanelMain.Controls.Add(this.buttonSample07);
            this.flowLayoutPanelMain.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelMain.Location = new System.Drawing.Point(185, 63);
            this.flowLayoutPanelMain.Name = "flowLayoutPanelMain";
            this.flowLayoutPanelMain.Padding = new System.Windows.Forms.Padding(10);
            this.flowLayoutPanelMain.Size = new System.Drawing.Size(334, 314);
            this.flowLayoutPanelMain.TabIndex = 3;
            // 
            // buttonSample01
            // 
            this.buttonSample01.AutoSize = true;
            this.buttonSample01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSample01.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSample01.Location = new System.Drawing.Point(13, 13);
            this.buttonSample01.MinimumSize = new System.Drawing.Size(210, 36);
            this.buttonSample01.Name = "buttonSample01";
            this.buttonSample01.Size = new System.Drawing.Size(308, 36);
            this.buttonSample01.TabIndex = 0;
            this.buttonSample01.Text = "Sample 1 - Just message!+DefaultCulture";
            this.buttonSample01.UseVisualStyleBackColor = true;
            this.buttonSample01.Click += new System.EventHandler(this.ButtonSample01_Click);
            // 
            // buttonSample02
            // 
            this.buttonSample02.AutoSize = true;
            this.buttonSample02.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSample02.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSample02.Location = new System.Drawing.Point(13, 55);
            this.buttonSample02.MinimumSize = new System.Drawing.Size(210, 36);
            this.buttonSample02.Name = "buttonSample02";
            this.buttonSample02.Size = new System.Drawing.Size(308, 36);
            this.buttonSample02.TabIndex = 1;
            this.buttonSample02.Text = "Sample 2 - Message+InformationCaption+fa-IR";
            this.buttonSample02.UseVisualStyleBackColor = true;
            this.buttonSample02.Click += new System.EventHandler(this.ButtonSample02_Click);
            // 
            // buttonSample03
            // 
            this.buttonSample03.AutoSize = true;
            this.buttonSample03.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSample03.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSample03.Location = new System.Drawing.Point(13, 97);
            this.buttonSample03.MinimumSize = new System.Drawing.Size(210, 36);
            this.buttonSample03.Name = "buttonSample03";
            this.buttonSample03.Size = new System.Drawing.Size(308, 36);
            this.buttonSample03.TabIndex = 2;
            this.buttonSample03.Text = "Sample 3 - MessagePlus+InformationCaption";
            this.buttonSample03.UseVisualStyleBackColor = true;
            this.buttonSample03.Click += new System.EventHandler(this.ButtonSample03_Click);
            // 
            // buttonSample04
            // 
            this.buttonSample04.AutoSize = true;
            this.buttonSample04.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSample04.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSample04.Location = new System.Drawing.Point(13, 139);
            this.buttonSample04.MinimumSize = new System.Drawing.Size(210, 36);
            this.buttonSample04.Name = "buttonSample04";
            this.buttonSample04.Size = new System.Drawing.Size(308, 36);
            this.buttonSample04.TabIndex = 3;
            this.buttonSample04.Text = "Sample 4 - AbortRetryIgnore";
            this.buttonSample04.UseVisualStyleBackColor = true;
            this.buttonSample04.Click += new System.EventHandler(this.ButtonSample04_Click);
            // 
            // buttonSample05
            // 
            this.buttonSample05.AutoSize = true;
            this.buttonSample05.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSample05.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSample05.Location = new System.Drawing.Point(13, 181);
            this.buttonSample05.MinimumSize = new System.Drawing.Size(210, 36);
            this.buttonSample05.Name = "buttonSample05";
            this.buttonSample05.Size = new System.Drawing.Size(308, 36);
            this.buttonSample05.TabIndex = 4;
            this.buttonSample05.Text = "Sample 5 - NoIcon";
            this.buttonSample05.UseVisualStyleBackColor = true;
            this.buttonSample05.Click += new System.EventHandler(this.ButtonSample05_Click);
            // 
            // buttonSample06
            // 
            this.buttonSample06.AutoSize = true;
            this.buttonSample06.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSample06.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSample06.Location = new System.Drawing.Point(13, 223);
            this.buttonSample06.MinimumSize = new System.Drawing.Size(210, 36);
            this.buttonSample06.Name = "buttonSample06";
            this.buttonSample06.Size = new System.Drawing.Size(308, 36);
            this.buttonSample06.TabIndex = 5;
            this.buttonSample06.Text = "Sample 6 - WindowIcon";
            this.buttonSample06.UseVisualStyleBackColor = true;
            this.buttonSample06.Click += new System.EventHandler(this.ButtonSample06_Click);
            // 
            // buttonSample07
            // 
            this.buttonSample07.AutoSize = true;
            this.buttonSample07.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSample07.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSample07.Location = new System.Drawing.Point(13, 265);
            this.buttonSample07.MinimumSize = new System.Drawing.Size(210, 36);
            this.buttonSample07.Name = "buttonSample07";
            this.buttonSample07.Size = new System.Drawing.Size(308, 36);
            this.buttonSample07.TabIndex = 6;
            this.buttonSample07.Text = "Sample 7 - Long Message";
            this.buttonSample07.UseVisualStyleBackColor = true;
            this.buttonSample07.Click += new System.EventHandler(this.ButtonSample07_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 441);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(720, 480);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HiVE.MsgBoxSelector - Demo [WinForms]";
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.flowLayoutPanelMain.ResumeLayout(false);
            this.flowLayoutPanelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelMain;
        private System.Windows.Forms.Button buttonSample01;
        private System.Windows.Forms.Button buttonSample02;
        private System.Windows.Forms.Button buttonSample03;
        private System.Windows.Forms.Button buttonSample04;
        private System.Windows.Forms.Button buttonSample05;
        private System.Windows.Forms.Button buttonSample06;
        private System.Windows.Forms.Button buttonSample07;
    }
}

