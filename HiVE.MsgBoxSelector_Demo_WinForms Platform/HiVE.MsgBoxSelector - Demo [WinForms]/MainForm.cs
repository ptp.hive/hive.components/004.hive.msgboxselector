﻿using HiVE.MsgBoxSelector;
using HiVE.MsgBoxSelector_Demo_WinForms.Models;
using System.Windows.Forms;
using static HiVE.MsgBoxSelector.MsgBoxSpecialMethods;
using static HiVE.MsgBoxSelector_Demo_WinForms.Models.SpecialModels;

namespace HiVE.MsgBoxSelector_Demo_WinForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        #region Feilds

        private static string _caption = AssemblyAttributeAccessors.ExecutingAssemblyProduct;

        private static string _message_En = TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString();
        private static string _message_Fa = TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString();

        private static string _messagePlus_En =
            TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToSpecialDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToSpecialDescriptionString() +
            "\n";

        private static string _messagePlus_Fa =
            TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n" + TcMethodGeneralErrorMessage.LogicError.ToDescriptionString() + " " + TcMethodGeneralErrorMessage.ShowBlockingDialog.ToDescriptionString() +
            "\n";

        private static string _longMessage_En =
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En +
            _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En + _messagePlus_En;

        private static string _longMessage_Fa =
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa +
            _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa + _messagePlus_Fa;

        #endregion

        #region StackPanel Sample MsgBoxSelector - Default

        private void ButtonSample01_Click(object sender, System.EventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _message_En,
                    MsgBoxMethodCulture.Default.ToString()
                    );
            }
            catch { }
        }

        private void ButtonSample02_Click(object sender, System.EventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _message_Fa,
                    MsgBoxMethodCaption.Information.ToString(),
                    MsgBoxMethodButtons.OKCancel,
                    MsgBoxMethodIcon.Shield,
                    MsgBoxMethodDefaultButton.Button1,
                    MsgBoxMethodCulture.fa_IR.ToString()
                    );
            }
            catch { }
        }

        private void ButtonSample03_Click(object sender, System.EventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _messagePlus_En,
                    MsgBoxMethodCaption.Information.ToString(),
                    MsgBoxMethodButtons.OKCancel,
                    MsgBoxMethodIcon.Shield,
                    MsgBoxMethodDefaultButton.Button1,
                    MsgBoxMethodCulture.en_US.ToString()
                    );
            }
            catch { }
        }

        private void ButtonSample04_Click(object sender, System.EventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    "First Message _ Text",
                    "Caption",
                    MsgBoxMethodButtons.AbortRetryIgnore,
                    MsgBoxMethodIcon.Stop,
                    MsgBoxMethodCulture.en_US.ToString()).ToString();
            }
            catch { }
        }

        private void ButtonSample05_Click(object sender, System.EventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _messagePlus_Fa,
                    _caption,
                    MsgBoxMethodButtons.OKCancel,
                    MsgBoxMethodCulture.fa_IR.ToString());
            }
            catch { }
        }

        private void ButtonSample06_Click(object sender, System.EventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _messagePlus_En,
                    _caption,
                    MsgBoxMethodButtons.ReconnectExit,
                    MsgBoxMethodIcon.ApplicationIcon,
                    MsgBoxMethodCulture.en_US.ToString());
            }
            catch { }
        }

        private void ButtonSample07_Click(object sender, System.EventArgs e)
        {
            try
            {
                MessageBoxSelector.Show(
                    _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En +
                    "\n" + _longMessage_En
                    , MsgBoxMethodCulture.Default.ToString());
            }
            catch { }
        }

        #endregion
    }
}
