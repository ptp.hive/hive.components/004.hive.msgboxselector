﻿using System.ComponentModel;

namespace HiVE.MsgBoxSelector_Demo_WinForms.Models
{
    internal static class SpecialModels
    {
        public enum TcMethodGeneralErrorMessage
        {
            [SpecialDescription("Logic Error!")]
            [Description("خطای منطقی.")]
            LogicError,
            [SpecialDescription("An unexpected error!")]
            [Description("یک خطای غیر منتظره!")]
            UnexpectedError,

            [SpecialDescription("Please close all message boxes opened by this application, before using this window!")]
            [Description("لطفا تمام جعبه پیام باز شده توسط این نرم افزار را ببندید، قبل از استفاده از این پنجره!")]
            ShowBlockingDialog,
            [SpecialDescription("Could not extract default icon for MessageBox Window!")]
            [Description("نماد پیش فرض برای پیام جعبه پنجره استخراج نشده!")]
            CouldNotExtractIcon,

            [Description("خطا در بارگیری برنامه.")]
            LoadingWindowError,
            [Description("خطا در بارگیری برنامه.")]
            LoadingUserControlError,

            [SpecialDescription("Error loading input file path!")]
            [Description("خطا در بارگیری مسیر فایل ورودی.")]
            LoadingInputFilePathError,

            [Description("خطا در ایجاد پایگاه‌داده.")]
            ErrorModelCreatingDatabase,
            [Description("خطا در مقداردهی اولیه به اطلاعات پایگاه‌داده.")]
            ErrorSeedInitialValue,
            [Description("خطا در برقراری ارتباط با پایگاه‌داده.")]
            ErrorConnectionDatabase,
            [Description("خطا در بارگیری اطلاعات صندوق.")]
            ErrorLoadInformationsDatabase,

            [Description("{0} با این {1} پیدا نشد!")]
            ErrorNotFoundWithThis_2
        }

    }
}
