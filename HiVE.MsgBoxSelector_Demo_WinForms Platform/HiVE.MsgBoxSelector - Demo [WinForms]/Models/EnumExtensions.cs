﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace HiVE.MsgBoxSelector_Demo_WinForms.Models
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Get Description of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToDescriptionString<T>(this T value)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }

        /// <summary>
        /// Get Especially Description of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSpecialDescriptionString<T>(this T value)
        {
            SpecialDescriptionAttribute[] attributes = (SpecialDescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(SpecialDescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].SpecialDescription : string.Empty;
        }

        /// <summary>
        /// Get Enum Member Index
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <returns></returns>
        public static int GetEnumMemberIndex<T>(T element)
            where T : struct
        {
            T[] values = (T[])Enum.GetValues(typeof(T));
            return Array.IndexOf(values, element);
        }

        /// <summary>
        /// Get Enum Value from string Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public static T GetEnumValue<T>(string strValue) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }
            T val;
            return Enum.TryParse<T>(strValue, true, out val) ? val : default(T);
        }

        /// <summary>
        /// Get Enum Value from int Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="intValue"></param>
        /// <returns></returns>
        public static T GetEnumValue<T>(int intValue) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }

            return (T)Enum.ToObject(enumType, intValue);
        }

        public static IEnumerable<Enum> GetEnumValues(Enum enumeration)
        {
            return from f in enumeration.GetType().GetFields(BindingFlags.Static | BindingFlags.Public)
                   select (Enum)f.GetValue(enumeration);
        }

        /// <summary>
        /// Client Utils _ Is Enum Valid
        /// </summary>
        /// <param name="enumValue"></param>
        /// <param name="value"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static bool ClientUtils_IsEnumValid(Enum enumValue, int value, int minValue, int maxValue) =>
            ((value >= minValue) && (value <= maxValue));

        /// <summary>
        /// Windows Forms Utils _ Enum Validator _ Is Enum With in Shifted Range
        /// </summary>
        /// <param name="enumValue"></param>
        /// <param name="numBitsToShift"></param>
        /// <param name="minValAfterShift"></param>
        /// <param name="maxValAfterShift"></param>
        /// <returns></returns>
        private static bool WindowsFormsUtils_EnumValidator_IsEnumWithinShiftedRange(Enum enumValue, int numBitsToShift, int minValAfterShift, int maxValAfterShift)
        {
            int num = Convert.ToInt32(enumValue, CultureInfo.InvariantCulture);
            int num2 = num >> numBitsToShift;
            if ((num2 << numBitsToShift) != num)
            {
                return false;
            }
            return ((num2 >= minValAfterShift) && (num2 <= maxValAfterShift));
        }
    }
}
